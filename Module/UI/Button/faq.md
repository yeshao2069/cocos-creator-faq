## Cocos Creator FAQ
### Button / 按钮

---
### Q1
#### Question: Cocos Creator 3.x 版本的按钮，怎么实现事件穿透效果
[demo](./AllowButtonEventPass)
Answer: 需要设置按钮的可以穿透 ，另外需要重写按钮来实现。   
    
    1 获取到Button的节点，然后设置swallowTouches属性，默认为true，需要设置为false。
        let node = find('Canvas/Button')!;
        node.eventProcessor.touchListener.setSwallowTouches(false);

    2 重写按钮    
    
https://gitee.com/yeshao2069/cocos-creator-faq/blob/master/FAQ/AllowButtonEventPass/assets/test.ts#L35-122