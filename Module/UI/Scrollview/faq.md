## Cocos Creator FAQ
### Scrollview / 滚动视图

---
### Q1
#### Cocos Creator 如何查看 swallowTouches 属性？
A: 对于 UI 组件来说，以 Scrollview 为例，通过
    let node = find('Canvas/ScrollView')!;
    node.eventProcessor.touchListener['swallowTouches'];

对于节点 Node 来说，如果节点没有事件监听的话，那么获取的是 null，需要通过给节点 node 添加事件才可以正确获取。