## Cocos Creator FAQ
### Sprite / 图片

---
### Q1
#### Question: Cocos Creator 3.x 使用大平面作为地形移动时边界会有闪烁，怎么处理?
Answer:    

查看 plane 平面的材质的贴图，设置贴图纹理的 mip filter 设置为 linear。

---
### Q2
#### Question: Cocos Creator 3.x 中纹理的过滤模式 Point 对应什么?
Answer:    

在 2.4.x 中，纹理的过滤模式有 Point、Bilinear、Trilinear 等，在 3.x 版本没有这个了，可以配置纹理的 min filter 选项为 nearest 即可，可以有效解决 tieldmap 地形在缩放界面后有拼接线的问题。

3.x 纹理过滤设置的名称从 Point 改成了 OpenGL 的 NEAREST https://forum.cocos.org/t/topic/122430/2

---
### Q3
#### Question: 纹理设置只显示指定的矩形区域
Answer: 
> 方案1 使用 Mask 组件。    
> 方案2 this.sprite.rect = new Rect(10,10,10,10); 图片要禁用动态合图和禁用自动图集。

---
### Q4
#### Question: shader 实现的水纹不连续
R: Cocos Creator 2.4.6，水纹使用几张图拼接的，水纹不连续。

Answer: 可能是 uv 取值取到了边界了，胡出现波纹计算获取到空像素的情况，可以尝试将纹理设置为 repeat 模式。可以考虑使用整张图做 repeat。如果因为使用整图(背景图很大)，就需要纹理和纹理边距需要做一下重叠，重叠区域宽度为波的振幅。图片与图片之间的缝隙是因为 uv 浮动的范围超过了 0 到 1 之外，所以会采样空白像素即黑色，这个需要评估下，因为只有repeat 模式的采样纹理才会连续，不然就需要图片边缘重叠来掩盖黑色区域。 

---
### Q5 
#### Question: 截图下载下来的图片不同机型不一致
R: CocosCreator 2.4.6，实现了一个截图下载的功能，需要下载的图片大小为 640x480。预想是通过改变摄像机的 zoom ratio 的方式来使得截图下来的部分能充满 640x480.    

A: 参考代码
```
onSavePic () {
  let width = this.drawNode.width;
  let height = this.drawNode.height;
  let windowSize = cc.view.getVisibleSize();
  this.capCamera.zoomRatio = 2.3 * windowSize.height / 1136;
  
  let texture = new cc.RenderTexture();
  texture.initWithSize(width, height, cc.RenderTexture.DepthStencilFormat.RB_FMT_S8);
  this.capCamera.targetTexture = texture;

  let canvas: HTMLCanvasElement = document.createElement('canvas');
  canvas.width = width;
  canvas.height = height;
  let ctx = canvas.getContext('2d');
  this.capCamera.render();
  let data = texture.readPixels();
  let rowBytes = width * 4;
  for (let row = 0; row < height; row++) {
      let srow = height - 1 - row;
      let imageData = ctx.createImageData(width, 1);
      let start = srow * width * 4;
      for (let i = 0; i < rowBytes; i++) {
          imageData.data[i] = data[start + i];
      }
      ctx.putImageData(imageData, 0, row);
  }
  let dataUrl = canvas.toDataURL('image/jpg');ß

  let saveLink: any = document.createElementNS('http://www.w3.org/1999/xhtml', 'a');
  saveLink.href = dataUrl;
  saveLink.download = String(Date.now()) + '.jpg';
  let event = document.createEvent('MouseEvents');
  event.initMouseEvent('click', true, false, window, 0, 0, 0, 0, 0, false, false, false, false, 0, null);
  saveLink.dispatchEvent(event);
  this.scheduleOnce((t: any) => {
      this.capCamera.enabled = false;
  }, 0.1);
}
```   