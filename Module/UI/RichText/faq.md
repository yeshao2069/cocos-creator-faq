## Cocos Creator FAQ
### RichText / 富文本

---
### Q1
#### Question: Cocos Creator 3.x 富文本组件(Editbox) 设置 PLACEHOLDER_LABEL 的行高无效?
Answer:    

由于 PLACEHOLDER_LABEL 的Label的 OverFlow 设置为 CLAMP模式 导致的问题，可以设置为 NONE模式 或者 RESIZE_HEIGHT 模式即可调整行高。  @zmzczy https://forum.cocos.org/t/topic/126456/9