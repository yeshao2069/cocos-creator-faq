## Cocos Creator FAQ
### Optimization / 优化

---
### 1
- 咨询: resources.load 控制并发加载?
#### Version: Cocos Creator 3.4.0
Question Addtion:    

Cocos Creator 3.4.0, 在低端安卓测试机上测试, 使用 resources.load 加载 100 多个预制体 prefab, 会导致 websocket 断开, 貌似 CPU 被耗尽了。尝试先加载 50 个 prefab 后再加载剩余的 50 个 prefab 后, 情况变好一些。所以有没有控制 resources.load 并发。低端机型(OPPO A5、VIVOx21)等、非必现的情况, 在小游戏平台没问题, 但是在低端安卓机型可能存在卡死(websocket 断开了), 甚至闪退。    

A: reources.load 目前引擎没有可以控制并发加载的处理。可以考虑采用分帧加载的方式规避。另外还有一些可以参考的方案，可以尝试: 
> 1. 把需要的资源放到场景里，然后加载场景.     
> 2. 尝试减少并发数(修改 assetManager.downloader.maxConcurrency 的数值).    
> 3. 采用分帧加载