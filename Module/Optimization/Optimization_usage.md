## Cocos Creator FAQ
### Optimization / 优化

---
### 1
项目优化 | 内存优化 | Cocos Creator 3.x

* 3.X 3D 项目内存优化

    - 检查模型使用的纹理是否有分辨率溢出，如果有，请批量降低纹理的分辨率
    - 优化材质，去掉不必要的贴图或者合并贴图
    - 使用GPU纹理格式
    - 降低模型的面数
    - 合理使用模型的instancing，instance虽然可以提高渲染性能，但是也会带来内存少量增加
    - 检查纹理的mipmap，纹理texture的mipmap会导致小部分内存增加
    - 图集的大图填充率低会导致内存浪费
    - 音频资源可以减少采样率来减少内存占用
    - 开启动态合图会占用额外内存，对于UI元素不多的游戏可以关闭
    - 使用3D粒子屏幕外剔除功能(CocosCreator3.4.0已支持，3.3.x版本可以合并PR https://github.com/cocos-creator/engine/pull/9349/files来支持，3.3.0版本以下的版本可能不支持)
    - 可能没有及时释放不用的资源，也可能没有及时调用GC来快速释放项目内存
    - 可以使用的文章以及工具 
    > https://www.soft8soft.com/docs/manual/zh/introduction/Optimizing-WebGL-performance.html
    > https://mp.weixin.qq.com/s/g22ealeBbHhUOGGlf0oWBw

---
### 2
项目优化 | 内存优化 & 性能优化 | Cocos Creator 3.x

* 3.X 项目优化 - 内存

    - 检查模型使用的纹理是否有分辨率溢出，有的话，批量降低
    - 优化材质，去掉不必要的贴图
    - 使用GPU纹理格式
    - 降低模型面数

* 3.X 项目优化 - 性能

    - 粒子尽量使用GPU格式， 优先使用内置的面片，不行才用Mesh
    - 优化材质，去掉不必要的贴图
    - 开启GPU Instancing
    - 降低DrawCall
    - 确保高端机效果最优、针对中、低端机采用降低细节和效果的方式实现
    - 骨骼数量优化 (确保效果OK的情况下，骨骼数量越少，性能越优)
    - 阴影，如果不需要，请确保阴影是关闭的
    - 屏幕外粒子剔除(CocosCreator3.4.0已支持，3.3.x版本可以合并PR https://github.com/cocos-creator/engine/pull/9349/files来支持，3.3.0版本以下的版本可能不支持)

---
### 3
项目优化 | 资源管理

@宝爷 https://forum.cocos.org/t/cocos-creator/84793

[方案](./OptimizationResourceManager/ResourceManager-I.md)

---
### U4
编辑器卡顿优化 | Cocos Creator 2.x

https://forum.cocos.org/t/topic/115111