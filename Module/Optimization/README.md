### Optimization / 优化
* 简介
    
    优化，通常指的是对于项目进行优化，包括性能优化、内存优化、资源管理、稳定性优化等。

| 编号 | 版本 | 子项 | 备注 |
| :---: | :--- | :---: |
| 1 | All | [项目优化 / 内存优化](./Optimization_usage.md#1) | 进阶优化 |
| 2 | All | [项目优化 / 内存优化&性能优化](./Optimization_usage.md#2) | 进阶优化 |
| 3 | All | [项目优化 / 资源管理](./Optimization_usage.md#3) | 进阶优化 |
| 4 | 2.x | [编辑器卡顿优化](./Optimization_usage.md#4) | 进阶优化 |
| 5 | All | [resources.load 控制并发加载](./Optimization_faq.md#1) | FAQ问答 |