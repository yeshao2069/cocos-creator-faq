## Cocos Creator FAQ
### Resource - 资源管理

---
### U1
#### Title: 加载 resources 预制体

```typescript
// 2.x
// cc.resources.load("test assets/prefab", function (err, prefab) {
//     var newNode = cc.instantiate(prefab);
//     cc.director.getScene().addChild(newNode);
// });

// 3.x 
import { resources, instantiate, Node } from 'cc';
resources.load("test assets/prefab", Prefab, (err, prefab) => {
    const newNode = instantiate(prefab);
    this.node.addChild(newNode);
});
```

---
### U2
#### Title: 加载 resources 动画片段

```typescript
// 2.x
// var self = this;
// cc.resources.load("test assets/anim", function (err, clip) {
//     self.node.getComponent(cc.Animation).addClip(clip, "anim");
// });

// 3.x 
import { resources, AnimationClip, Animation } from 'cc';
resources.load("test assets/anim", AnimationClip, (err, clip) => {
    this.node.getComponent(Animation).addClip(clip, "anim");
});
```

---
### U3
#### Title: 加载 resources 图片

```typescript
// 如果第二个参数 cc.SpriteFrame 没有指定，那么直接加载 test assets/image 获取的类型是 cc.Texture2D。
// 需要不想指定第二个参数 cc.SpriteFrame，又需要获取 cc.SpriteFrame 类型，需要在资源管理器将图片类型设置为 Sprite。 
// 2.x
// var self = this;
// cc.resources.load("test assets/image", cc.SpriteFrame, function (err, spriteFrame) {
//     self.node.getComponent(cc.Sprite).spriteFrame = spriteFrame;
// });

// 需要在资源管理器将图片的类型设置为 SpriteFrame, 资源中会生成 SpriteFrame 类型资源。否则加载会是空值。
// 直接加载 test assets/image， 得到的类型将会是 ImageAsset 类型。
// 3.x 
import { resources, SpriteFrame, Sprite } from 'cc';
resources.load("test assets/image/spriteFrame", SpriteFrame, (err, spriteFrame) => {
    let spf = this.node.getComponent(Sprite).spriteFrame;
    const spriteFrame = new SpriteFrame();
    spriteFrame.setTexture(texture, spf.getRect(), spf.isRotated(), spf.getOffset(), spf.getOriginalSize()); 
    this.node.getComponent(Sprite).spriteFrame = spriteFrame;
});
```

---
### U4
#### Title: 加载 resources 纹理

```typescript
// 如果第二个参数 cc.SpriteFrame 没有指定，那么直接加载 test assets/image 获取的类型是 cc.Texture2D。
// 需要不想指定第二个参数 cc.SpriteFrame，又需要获取 cc.SpriteFrame 类型，需要在资源管理器将图片类型设置为 Sprite。 
// 2.x
// var self = this;
cc.resources.load("test assets/image", function (err, texture) {
    self.node.getComponent(cc.Sprite).spriteFrame = spriteFrame;
});

// 3.x 
import { resources, Texture2D, SpriteFrame, Sprite } from 'cc';
resources.load("test assets/image/texture", Texture2D, (err: any, texture: Texture2D) => {
    const spriteFrame = new SpriteFrame();
    spriteFrame.texture = texture;
    this.node.getComponent(Sprite).spriteFrame = spriteFrame;
});
```