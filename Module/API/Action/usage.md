## Cocos Creator FAQ
### Action - 动作

---
### U1
#### Title: 立即显示

```typescript
// 2.x
// var showAction = cc.show();
// node.runAction(showAction);

// 3.x
tween(node).show();
```

---
### U2
#### Title: 立即隐藏

```typescript
// 2.x
// var hideAction = cc.hide();
// node.runAction(hideAction);

// 3.x
tween(node).hide();
```

---
### U3
#### Title: 显隐切换

```typescript
// 2.x
// var toggleVisibilityAction = cc.toggleVisibility();
// node.runAction(toggleVisibilityAction);

// 3.x
import { RenderComponent } from 'cc';
var renders = target.getComponentsInChildren(RenderComponent);
for (var i = 0; i < renders.length; i++) {
    var render = renders[i];
    render.enabled = !render.enabled;
}
```

---
### U4
#### Title: 渐显效果

```typescript
// 2.x
// var action = cc.fadeIn(1.0);
// node.runAction(action);

// 3.x
import { RenderComponent } from 'cc';
let c = new Vec4();
let renders = target.getComponentsInChildren(RenderComponent);

for(let i = 0; i < renders.length; i++) {
    c.x = renders[i].color.r;
    c.y = renders[i].color.g;
    c.z = renders[i].color.b;
    c.w = renders[i].color.a;

    let tc = new Color();
    tween(c).to(1.0, new Vec4(c.x, c.y, c.z, 255), {
        'onUpdate' : () => {
            tc.r = c.x;
            tc.b = c.y;
            tc.g = c.z;
            tc.a = c.w;
            renders[i].color = tc;
        }  
    }).start();
}
```

---
### U5
#### Title: 渐隐效果

```typescript
// 2.x
// var action = cc.fadeOut(1.0);
// node.runAction(action);

// 3.x
import { RenderComponent } from 'cc';
let c = new Vec4();
let renders = target.getComponentsInChildren(RenderComponent);

for(let i = 0; i < renders.length; i++) {
    c.x = renders[i].color.r;
    c.y = renders[i].color.g;
    c.z = renders[i].color.b;
    c.w = renders[i].color.a;

    let tc = new Color();
    tween(c).to(1.0, new Vec4(c.x, c.y, c.z, 0), {
        'onUpdate' : () => {
            tc.r = c.x;
            tc.b = c.y;
            tc.g = c.z;
            tc.a = c.w;
            renders[i].color = tc;
        }  
    }).start();
}
```