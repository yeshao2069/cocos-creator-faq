## Cocos Creator FAQ
### Scene - 场景

---
### U1
#### Title: 场景跳转

```typescript
// 2.x
//cc.director.loadScene('sceneName');

// 3.x 
import { director } from 'cc';
director.loadScene('sceneName');
```

---
### U2
#### Title: 预加载场景  

```typescript
// 2.x
// cc.director.preloadScene('sceneName');

// 3.x
import { director } from 'cc';
director.preloadScene('sceneName');
```

---
### U3
#### Title: 获取当前场景

```typescript
// 2.x
// cc.director.getScene();

// 3.x
import { director } from 'cc';
director.getScene();
```

---
### U4
#### Title: 动态关闭阴影

```typescript
// 3.x
import { director } from 'cc';
director.getScene().globals.shadows.enabled = false;
```