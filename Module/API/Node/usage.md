## Cocos Creator FAQ
### Node - 节点

---
### U1
#### Title: 访问节点<绝对路径>

```typescript
// 2.x
// var node = cc.find("Canvas/bg");

// 3.x
import { Node, find } from 'cc';
var node = find("Canvas/bg");
```

---
### U2
#### Title: 节点上的组件的属性值<设置/获取>

```typescript
// 2.x
// node.getComponent(cc.Label).string = 'abc';
// var str = node.getComponent(cc.Label).string;

// 3.x
import { Node, Label } from 'cc';
node.getComponent(Label).string = 'abc';
var str = node.getComponent(Label).string;
```

---
### U3
#### Title: 获取节点以及指定组件<访问路径>

```typescript
// 2.x
// var sprite = cc.find("Canvas/bg").getComponent(cc.Sprite);

// 3.x
import { Node, find, Sprite } from 'cc';
var sprite = find("Canvas/bg")!.getComponent(Sprite);
```

---
### U4
#### Title: 获取子节点以及指定组件<节点名>

```typescript
// 2.x
// var label = this.node.getChildByName('name').getComponent(cc.Label);

// 3.x
import { Node, Label } from 'cc';
var label = this.node.getChildByName('name').getComponent(Label);
```

---
### U5
#### Title: 获取当前脚本所在的节点

```typescript
var node = this.node;
```

---
### U6
#### Title: 获取父节点

```typescript
var node = this.node.parent;
```

---
### U7
#### Title: 获取子节点

```typescript
// 2.x
// var child = this.node.getChildByUuid('77G0JoyLpAtqdIg6i4n4am'); // 方式1
// var child = this.node.getChildByName('name'); // 方式2

// 3.x
import { Node } from 'cc';
var child = this.node.getChildByUuid('77G0JoyLpAtqdIg6i4n4am'); // 方式1
var child = this.node.getChildByPath('subNode/node'); // 方式2
var child = this.node.getChildByName('name'); // 方式3
```

---
### U8
#### Title: 获取指定路径的节点<相对路径>

```typescript
// 2.x
// var child = cc.find("bg/score", this.node);

// 3.x
import { Node, find } from 'cc';
var child = find("bg/score", this.node);
```

---
### U9
#### Title: 获取所有子节点

```typescript
var childs = this.node.children;
```

---
### U10
#### Title: 获取子节点数量

```typescript
// 2.x
// var len = this.node.childrenCount;

// 3.x
import { Node } from 'cc';
var len = this.node.children.length;
```

---
### U11
#### Title: 克隆节点

```typescript
// 2.x
// var child = cc.instantiate(node);

// 3.x
import { Node, instantiate } from 'cc';
var child = instantiate(node);
```

---
### U12
#### Title: 绑定父节点

```typescript
// 2.x
// this.node.parent = cc.find('Canvas');

// 3.x
import { Node, find } from 'cc';
this.node.parent = find('Canvas');
```

---
### U13
#### Title: <添加/移除>子节点

```typescript
// 2.x
// this.node.addChild(node,zIndex,tag); // 添加子节点
// this.node.addChild(node);
// this.node.removeChild(node); // 移除子节点

// 3.x
import { Node } from 'cc';
this.node.addChild(node); // 添加子节点
this.node.removeChild(node); // 移除子节点
```

---
### U14
#### Title: 销毁节点

```typescript
this.node.destroy();
```

---
### U15
#### Title: 判定节点是否可用

```typescript
this.node.isValid;

// 由于节点的 destroy 操作是下一帧执行的。
// 如果当前节点调用 node.destory，如果直接通过 node.isValid 去判断还是 true
// 需要使用 cc.isValid(node) 去判断

// 2.x
// cc.isValid(this.node);

// 3.x
import { isValid } from 'cc';
isValid(this.node);
```

---
### U16
#### Title: <移除/销毁>所有子节点

```typescript
this.node.removeAllChildren();
this.node.destroyAllChildren();
```

---
### U17
#### Title: 停止所有正在播放的动作和计时器

```typescript
// 2.x
// this.node.cleanup();

3.x
import { Node, Tween, TweenSystem, systemEvent } from 'cc';
Tween.stopAllByTarget(this.node);
TweenSystem.instance.ActionManager.removeAllActionsFromTarget(this.node);
this.node.targetOff('eventA');
```

---
### U18
#### Title: <递归>查找自身以及所有指定组件

```typescript
// 2.x
// var sprites = this.node.getComponentsInChildren(cc.Sprite);

// 3.x
import { Node, Sprite } from 'cc';
var sprites = this.node.getComponentsInChildren(Sprite);
```

---
### U19
#### Title: 获取节点的坐标

```typescript
// 2.x
var x = this.node.getPositionX();
var y = this.node.getPositionY();

// 3.x
import { Node } from 'cc';
var pos = this.node.getPosition();
var x = this.node.getPosition().x;
var y = this.node.getPosition().y;
var z = this.node.getPosition().z;
```

---
### U20
#### Title: 获取节点缩放比例<x/y轴>

```typescript
// 2.x
// var scalex = this.node.getScaleX();
// var scaley = this.node.getScaleY();

// 3.x
import { Node } from 'cc';
var scale = this.node.getScale();
var scalex = this.node.getScale().x;
var scaley = this.node.getScale().y;
var scalez = this.node.getScale().z;
```

---
### U21
#### Title: 设置节点的坐标

```typescript
// 2.x
// this.node.x = 100;
// this.node.y = 100;
// this.node.setPosition(100, 100);
// this.node.setPosition(cc.v2(100, 100));

// 3.x
import { Node, Vec3 } from 'cc';
this.node.setPosition(new Vec3(100, 100, 0)); // 方法1
this.node.setPosition(100, 100, 0); // 方法2
```

---
### U22
#### Title: 设置节点的坐标

```typescript
// 2.x
// this.node.rotationX = 90;
// this.node.rotationY = 90;

// 3.x
import { Node, Vec3 } from 'cc';
this.node.setRotationFromEuler(new Vec3(90, 90, 0)); // 方法1
this.node.setRotationFromEuler(90, 90, 0); // 方法2
```

---
### U23
#### Title: 设置节点的缩放倍数

```typescript
// 2.x
// this.node.scaleX = 2;
// this.node.scaleY = 2;
// this.node.scale = 2;
// this.node.setScale(2);

// 3.x
import { Node, Vec3 } from 'cc';
this.node.setScale(new Vec3(2, 2, 1)); // 方法1
this.node.setScale(2, 2, 1); // 方法2
```

---
### U24
#### Title: 设置节点<宽度/高度>

```typescript
// 2.x
// this.node.width = 100;
// this.node.height = 100;
// this.node.setContentSize(100, 100);

// 3.x
import { Node, Size, UITransform } from 'cc';
let uitrans = this.node.getComponent(UITransform) || this.node.addComponent(UITransform);
uitrans.setContentSize(new Size(100, 100)); // 方法1
uitrans.setContentSize(100, 100); // 方法2
```

---
### U25
#### Title: 设置节点的锚点坐标

```typescript
// 2.x
// this.node.anchorX = 1;
// this.node.anchorY = 0;
// this.node.setAnchorPoint(1, 0);

// 3.x
import { Node, Vec2, UITransform } from 'cc';
this.node.getComponent(UITransform)!.setAnchorPoint(new Vec2(1, 0)); // 方法1
this.node.getComponent(UITransform)!.setAnchorPoint(1, 0); // 方法2
```

---
### U26
#### Title: 设置节点透明度大小<0至255>

```typescript
// 2.x
// this.node.opacity = 128;
// this.node.setOpacity(20); 

// 3.x
import { Node, UIOpacity } from 'cc';
let uiop = this.node.getComponent(UIOpacity) || this.node.addComponent(UIOpacity);
uiop.opacity = 200; // 如果是节点的话，使用这种方式，需要节点上有UIOpacity组件
var oldColor = this.node.getComponent(Label)!.color; // 如果是带有渲染组件(Label、Sprite)等，使用这种方式
this.node.getComponent(Label)!.color = new Color(oldColor.r, oldColor.g, oldColor.b, 200);
```

---
### U27
#### Title: 判定节点是否存在

```typescript
// 2.x
// if (cc.isValid(this.label.node)){
//     // xxx
// }

// 3.x
import { Node, isValid } from 'cc';
if (isValid(this.label.node)){
    // xxx
}
```

---
### U28
#### Title: 隐藏节点

```typescript
this.node.active = false;
```

---
### U29
#### Title: 常驻节点

```typescript
// 2.x
// cc.game.addPersistRootNode(this.node); // 把当前节点设置为常驻节点
// cc.game.removePersistRootNode(this.node); // 取消当前节点为常驻节点

// 3.x
import { Node, game } from 'cc';
game.addPersistRootNode(this.node);
game.removePersistRootNode(this.node);
```