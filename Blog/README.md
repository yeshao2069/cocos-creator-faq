## 博客

* 简介
    
    Blog，博客。旨为教学或者解惑。

| 编号 | 类型 | 备注 |
| :---: | :--- | :--- |
| 1 | 如何生成一张默认的背景底图? | https://blog.csdn.net/u014206745/article/details/121457533?spm=1001.2014.3001.5501 |
| 2 | 图片不显示了？节点添加了没有显示？ | https://blog.csdn.net/u014206745/article/details/121510352?spm=1001.2014.3001.5501 |
| 3 | 图片导入到工程，没办法拖动到场景中? | https://blog.csdn.net/u014206745/article/details/121930698?spm=1001.2014.3001.5501 |
| 4 | 如何使用Android Studio连接MuMu模拟器 | https://blog.csdn.net/u014206745/article/details/122529498?spm=1001.2014.3001.5501 |
| 5 | 项目在 Web 预览 遇见 the server responded with a status of 404 (Not Found) 错误的处理 | https://blog.csdn.net/u014206745/article/details/122731035?spm=1001.2014.3001.5501 |
| 6 | Cocos Creator 2.x 自动打包（构建 + 编译） | https://blog.csdn.net/u014206745/article/details/125396640?spm=1001.2014.3001.5501 |
| 7 | 实现双人同屏效果 =＞ 攻略 | https://blog.csdn.net/u014206745/article/details/126117810?spm=1001.2014.3001.5501 |
| 8 | 实现场景小地图效果 =＞ 攻略 | https://blog.csdn.net/u014206745/article/details/126139328?spm=1001.2014.3001.5501 |
| 9 | 使用 UIMeshRenderer 组件实现 3D 渲染在 UI 上 =＞ 攻略 | https://blog.csdn.net/u014206745/article/details/126238474?spm=1001.2014.3001.5501 |
| 10 | SpriteFrame 设置偏移量（offset）是没效果了吗？ | https://blog.csdn.net/u014206745/article/details/126291113?spm=1001.2014.3001.5501 |
| 11 | 使用 RenderTexture 实现 3D 渲染在 UI 上 =＞ 攻略 | https://blog.csdn.net/u014206745/article/details/126363156?spm=1001.2014.3001.5501 |
| 12 | ShaderToy 中的 iTime 对应 Cocos Effect 中是什么？ | https://blog.csdn.net/u014206745/article/details/126372340?spm=1001.2014.3001.5501 |
| 13 | 代码报错了，各种代码爆红提示，太烦了 =＞ 关闭严格模式 | https://blog.csdn.net/u014206745/article/details/126426550?spm=1001.2014.3001.5501 |
| 14 | 解决安卓平台报错“xxx.androidProject::getNdkVersion” | https://blog.csdn.net/u014206745/article/details/126534718?spm=1001.2014.3001.5501 |
| 15 | 解决`类型为 “xxx“ 的表达式不能用于索引类型 “{}“。`的报错 | https://blog.csdn.net/u014206745/article/details/126579980?spm=1001.2014.3001.5501 |
| 16 | Shader 中的 layout(set = N, binding = M) 是什么意思？ | https://blog.csdn.net/u014206745/article/details/126584694?spm=1001.2014.3001.5501 |
| 17 | Cocos Creator 远程调试 =＞ 教程篇 | https://blog.csdn.net/u014206745/article/details/126715900?spm=1001.2014.3001.5501 |
| 18 | 如何快速开启实时阴影效果（shadow map 和 planar shadow） | https://blog.csdn.net/u014206745/article/details/127899173?spm=1001.2014.3001.5501 |
| 19 | 实现放大镜效果 | https://blog.csdn.net/u014206745/article/details/130064641?spm=1001.2014.3001.5501 |
