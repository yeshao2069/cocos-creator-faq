## 支持泰语换行

### 测试文本
แย่งชิงสมบัติของชาวบ้าน แถมยังทำลาย ได้รับชัยชนะ

-------

### 使用说明

- CocosCreator 2.4.x，使用 `SolvingThaiLineFeeds2X.js` 插件，放在项目`assets`目录下
- CocosCreator 3.x，需要自定义`engine`，把 text-utils.ts 替换到自定义引擎engine下，替换覆盖 engine/cocos/2d/utils/text-utils.ts