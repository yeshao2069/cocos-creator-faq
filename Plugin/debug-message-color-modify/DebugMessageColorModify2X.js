setTimeout(() => {
    const parentNode = cc.find('PROFILER-NODE');
    // 0: 白色 1: 红色 2: 绿色 3: 黄色 4: 蓝色 5: 天蓝色 6: 黑色
    const colorIdx = 3;
    const colors = [
        new cc.color(255, 255, 255, 255),
        new cc.color(255, 0, 0, 255),
        new cc.color(0, 255, 0, 255),
        new cc.color(255, 255, 0, 255),
        new cc.color(0, 0, 255, 255),
        new cc.color(0, 0, 0, 255)
    ];
    // 颜色
    parentNode.children[0].color = colors[colorIdx];
    parentNode.children[1].color = colors[colorIdx];
}, 500);