## Web 预览调式信息颜色修改

-------

### 使用说明

- CocosCreator 2.4.x，使用 `DebugMessageColorModify2X.js` 插件，放在项目`assets`目录下，勾选为插件
- CocosCreator 3.0.x ~ 3.8.x，使用 `DebugMessageColorModify3X.js` 插件，放在项目`assets`目录下