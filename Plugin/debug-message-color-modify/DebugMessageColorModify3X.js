
cc.game.once(cc.Game.EVENT_ENGINE_INITED, function () {
    // 0: 白色 1: 红色 2: 绿色 3: 黄色 4: 蓝色 5: 天蓝色 6: 黑色
    const colorIdx = 5;
    const colors = ['#fff', '#f00', '#0f0', '#ff0', '#00f', '#0ff', '#000']

    // 颜色
    cc.profiler._ctx.fillStyle = colors[colorIdx];

    // 刷新
    cc.profiler._statsDone = false;
    cc.profiler.generateStats();
});