## 允许 Spine 在编辑器运行

-------

### 使用说明

- CocosCreator 2.4.x，使用 `SupportSpineRunInEditor2X.js` 插件，放在项目`assets`目录下，勾选为插件，并允许编辑器加载
- CocosCreator 3.0.x ~ 3.7.x，使用 `SupportSpineRunInEditor3X.js` 插件，放在项目`assets`目录下
- CocosCreator 3.8.x，使用 `SupportSpineRunInEditor38X.js` 插件，放在项目`assets`目录下

### 链接
https://forum.cocos.org/t/topic/101524/7