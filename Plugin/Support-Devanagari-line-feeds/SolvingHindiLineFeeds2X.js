
/**
 * Listening for events after engine initialization
 * @author muxiandong 2021-10-29
 */
cc.game.once(cc.game.EVENT_ENGINE_INITED, () => {
    // @ts-ignore
    cc.textUtils.label_wordRex = /([a-zA-Z0-9ÄÖÜäöüßéèçàùêâîôûа-яА-ЯЁё\u0900-\u097F\uA8E0-\uA8FF]+|\S)/;
    // @ts-ignore
    cc.textUtils.label_lastWordRex = /([a-zA-Z0-9ÄÖÜäöüßéèçàùêâîôûаíìÍÌïÁÀáàÉÈÒÓòóŐőÙÚŰúűñÑæÆœŒÃÂãÔõěščřžýáíéóúůťďňĚŠČŘŽÁÍÉÓÚŤżźśóńłęćąŻŹŚÓŃŁĘĆĄ-яА-ЯЁё\u0900-\u097F\uA8E0-\uA8FF]+|\S)$/;
    // @ts-ignore
    cc.textUtils.label_lastEnglish = /[a-zA-Z0-9ÄÖÜäöüßéèçàùêâîôûаíìÍÌïÁÀáàÉÈÒÓòóŐőÙÚŰúűñÑæÆœŒÃÂãÔõěščřžýáíéóúůťďňĚŠČŘŽÁÍÉÓÚŤżźśóńłęćąŻŹŚÓŃŁĘĆĄ-яА-ЯЁё\u0900-\u097F\uA8E0-\uA8FF]+$/;
    // @ts-ignore
    cc.textUtils.label_firstEnglish = /^[a-zA-Z0-9ÄÖÜäöüßéèçàùêâîôûаíìÍÌïÁÀáàÉÈÒÓòóŐőÙÚŰúűñÑæÆœŒÃÂãÔõěščřžýáíéóúůťďňĚŠČŘŽÁÍÉÓÚŤżźśóńłęćąŻŹŚÓŃŁĘĆĄ-яА-ЯЁё\u0900-\u097F\uA8E0-\uA8FF]/;
});