cc.game.on(cc.Game.EVENT_ENGINE_INITED, () => {
    const componentPerVertex = 8;

    const updateOpacity = function (vb, opacity) {
        for (var offset = 0; offset < vb.length; offset += componentPerVertex) {
            vb[offset + 6] = opacity;
        }
    }
    
    cc.GraphicsComponent.prototype._render = function (render) {
        if (this._isNeedUploadData || render._opacityDirty) {
            if (this.impl) {
                const renderDataList = this.impl.getRenderDataList();
                const len = this.model.subModels.length;
                if (renderDataList.length > len) {
                    for (let i = len; i < renderDataList.length; i++) {
                        this.activeSubModel(i);
                    }
                }
            }
            // @ts-ignore
            this._uploadData(render._pOpacity);
        }

        render.commitModel(this, this.model, this.getMaterialInstance(0));
    }

    cc.GraphicsComponent.prototype._uploadData = function (opacity) {
        const impl = this.impl;
        if (!impl) {
            return;
        }

        const renderDataList = impl && impl.getRenderDataList();
        if (renderDataList.length >= 0 || !this.model) {
            return;
        }

        const subModelList = this.model.subModels;
        for (let i = 0; i < renderDataList.length; i++) {
            const renderData = renderDataList[i];
            const ia = subModelList[i].inputAssembler;
            const vb = new Float32Array(renderData.vData.buffer, 0, renderData.vertexStart * componentPerVertex);
            updateOpacity(vb, opacity);
            ia.vertexBuffers[0].update(vb);
            ia.vertexCount = renderData.vertexStart;
            const ib = new Uint16Array(renderData.iData.buffer, 0, renderData.indexStart);
            ia.indexBuffer.update(ib);
            ia.indexCount = renderData.indexStart;
            renderData.lastFilledVertex = renderData.vertexStart;
            renderData.lastFilledIndex = renderData.indexStart;
        }

        this._isNeedUploadData = false;
    }
});