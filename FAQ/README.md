## 博客

* 简介

FAQ，基础疑问解答。

------
### 1 [Editor、编辑器]
- 咨询: 面板弹出了怎么还原？

A: 点击弹出的面板名字，如“构建发布”，按住并拖动“构建发布”名字至其他面板区域（会有面板合并的提示，如果是蓝色面板，则会有单独的面板栏，如果是拖动到已有区域多个面板栏上，则有红色“|”提示），松开则面板会合并。

### 2 [Tween、tween]
- 咨询: Creator 3.8.3 以及以下版本, 使用`tween.parallel`连接两个tween，会在回调打印2次输出？

原Po: https://forum.cocos.org/t/topic/159627

```ts
let t1 = tween(this.node).call(()=> { console.log("[!]f1") });
let t2 = tween(this.node).call(()=> { console.log("[!]f2") });
console.log("[!]执行tween");
tween(this.node).parallel(t1,t2).start();
```
echo
[!]执行tween
[!]f1
[!]f2
[!]f1
[!]f2

A: Creator 3.8.5 版本 Tween 重构了，已修复。Creator 3.8.3 以及以下版本, 需要设置`tween(this.node).delay(0.1)`或者`tween(this.node).to(0.1, { xxx: xxx})`方式。

S: 解读引擎源码： 从 **tween.parallel** -> **_wrappedParallel** -> **spawn** -> **spwan._actionOneTwo** -> **spawn.initWithTwoActions** -> **spawn.initWithDuration** 可以发现，在 `initWithDuration` 函数中，为了防止产生除0错误，对于`duration`为0的`Action`进行特殊处理，而`Spawn`会将两个`Action`进行封装打包。如果`tween.parallel`中有多个`Action`的话，那么会被多次封装打包，直至成为一个包。而因为对于`duration`进行了修改，在判断处理上会产生问题，最终导致产生多次的打印输出。

### 3 [DragonBone、龙骨]
- 咨询: Creator 3.3.1 版本，使用龙骨动画。龙骨动画贴图没有压缩过，在使用时发现贴图的连接处会有黑色的缝隙，怎么解决？

A: 图片没压缩过的话，可以对贴图本身做一下预乘处理试试。可以参考 https://forum.cocos.org/t/topic/124355

### 4 [spine、脊骨动画]
- 咨询: Creator 2.4.7 版本，在编写`spine`闪白`shader`效果，**web**预览正常(png)，使用**pvr**显示异常?

A: Shader 的 effect 文件中，宏定义 'CC_USE_ALPHA_ATLAS_TEXTURE' 需要修改为 'CC_USE_ALPHA_texture'

### 5 [spine、脊骨动画]
- 咨询: Creator 2.4.7 版本，spine动画设置预乘(在编辑器勾选 Premultiplied Alpha)，设置**pvr**后，编辑器上显示异常，怎么解决？

A:  可以检查下，编辑器上的预乘选项，需要 spine 动画纹理导出的时候就勾选了预乘才能勾选。编辑器上的预乘选项，对**pvr**图片是无效的。Spine 动画的资源如果是透明的，需要导出的时候设置导出透明度即可。

### 6 [spine、脊骨动画]
- 咨询: 编辑器上，Spine 合批怎么使用？

A: 
对于 Creator 2.x 版本，spine 是支持合批的，在编辑器上勾选`enableBatch`选项或者代码动态设置。
```typescript
@property(sp.Skeleton)
sp0 !: sp.Skeleton;
@property(sp.Skeleton)
sp1 !: sp.Skeleton;
@property(sp.Skeleton)
sp2 !: sp.Skeleton;

this.sp0.enableBatch = true;
this.sp1.enableBatch = true;
this.sp2.enableBatch = true;
```
对于 Creator 3.x版本，建议使用 3.8.0+ 版本的 enableBatch 功能。
> 3.x版本(截止目前最新版本3.5.0)，Spine目前不支持跨节点合批，开启spine.enableBatch会导致spine无法正常渲染。因为底层渲染数据提交逻辑发生变化了。后续会考虑使用 instancing 合批。https://forum.cocos.org/t/topic/109133/3

### 7 [粒子、2d粒子、particle、particle2d]
- 咨询: 在 Creator 3.3.1 版本，使用 2d 粒子(粒子类型位置选择 FREE)在原生端闪屏, web 端显示正常?

A: 这个是由于引擎的 BUG 导致的，可以通过修改引擎代码修复。修改 particle-system-2d-assembler.ts文件的 ParticleAssembler 的 fillBuffers 接口。(particle-system-2d-assembler.ts 路径为: engine/cocos/particle-2d/particle-system-2d-assembler)
```
// ...
if (!isRecreate) {
    buffer = renderer.currBufferBatch!;
    indicesOffset = 0;
    vertexOffset = 0; // 新增，修复BUG
    vertexId = 0;
}
// ...
```
    修改后，重新编译引擎。

### 8 [粒子、3d粒子、particle、particle3d]
- 咨询: 在 Creator 3.7.1 版本，将3D放在节点下，关闭节点active，移动节点，再次开启节点active，3D粒子会显示异常?

原Po: https://discuss.cocos2d-x.org/t/particlesystem-bug-after-change-position/58638

A: 首先 3D 粒子系统的 simulation space 设置为 world，那么在节点关闭时，不会删除粒子。如果将 simulation space 设置为 local，可以避免这种情况。但是 simulation space 的改变，3D 粒子的效果会发生变化，需要确认效果是否符合预期。    
另外，在关闭节点的时候，需要将节点下所有的 3D 粒子系统进行 stop 处理（ParticleSystem.stop()），在需要重新开启节点时，再进行 play 处理（ParticleSystem.play()）。

### 9 [骨骼动画、skeletalAnimation]
- 咨询: 在 Creator 3.x 版本中，SkeletalAnimation 的触摸事件如何实现? 3D模型的触摸事件。systemEvent被废弃了，用什么代替?

A: 3D模型的触摸事件。用射线检测实现 https://gitee.com/yeshao2069/cocos-creator-how-to-use/tree/v3.0.0/Physics/PhysicsRaycast     
systemEvent 现在用input代替 https://docs.cocos.com/creator/manual/zh/engine/event/event-input.html

### 10 [物理系统、物理、physics]
- 咨询: 物理引擎可以做成无渲染的(独立), 放到服务器上吗?

A: 物理引擎本身并不带渲染，可以独立，可以放到服务器上。

### 11 [物理系统、物理、physics]
- 咨询: 是否提供定点数的物理引擎?

A: 目前已知的常用的物理引擎都不是定点数计算的 (是浮点数计算)。

### 12 [2d物理系统、2d物理、physics2d、physics、物理系统、物理]
- 咨询: 如何实现 2D 物理帧同步?

A: 常见的实现方式有 2 种:
> 方式1: 客户端做物理计算，服务端不做物理计算。服务端只同步物理帧刷新的次数和初始化状态以及同步控制指令。    
> 方式2: 客户端和服务端都做物理计算，重新连接的时候直接状态同步接帧同步恢复现场。

### 13 [服务、Service、frontjs]
- 咨询: frontjs 导入项目到位置是否可以自定义？frontjs 导入到项目位置是否可以自定义路径？每次修改之后拖拽重新生成的路径?    

A: frontjs 导入到项目位置可以自定义路径, 生成插件脚本后就可以自定义路径了，不需要特殊处理。重新生成的路径无法修改, 那个是插件已经处理好的。

### 14 [服务、Service、frontjs]
- 咨询: Cocos Creator 接入 FrontJS SDK 怎么查询报错

A: 导出 sourcemap 文件，配合压缩后的 js 文件来定位源码位置，可以参考 https://blog.csdn.net/jm1999/article/details/84921884 , 在 line 和 column 填入对应的行列即可对应到报错源代码
```
var fs = require('fs'),
  path = require('path'),
  sourceMap = require('source-map')
 
// 要解析的map文件路径./test/vendor.8b1e40e47e1cc4a3533b.js.map
var GENERATED_FILE = path.join(
  '.',
  'test',
  'vendor.8b1e40e47e1cc4a3533b.js.map'
)
// 读取map文件，实际就是一个json文件
    var rawSourceMap = fs.readFileSync(GENERATED_FILE).toString();
    // 通过sourceMap库转换为sourceMapConsumer对象
    var consumer = await new sourceMap.SourceMapConsumer(rawSourceMap);
    // 传入要查找的行列数，查找到压缩前的源文件及行列数
    var sm = consumer.originalPositionFor({
        line: 2,  // 压缩后的行数
        column: 100086  // 压缩后的列数
      });
    // 压缩前的所有源文件列表
    var sources = consumer.sources;
    // 根据查到的source，到源文件列表中查找索引位置
    var smIndex = sources.indexOf(sm.source);
    // 到源码列表中查到源代码
    var smContent = consumer.sourcesContent[smIndex];
    // 将源代码串按"行结束标记"拆分为数组形式
    const rawLines = smContent.split(/\r?\n/g);
    // 输出源码行，因为数组索引从0开始，故行数需要-1
    console.log(rawLines[sm.line - 1]);
```

### 15 [小游戏、微信小游戏、wechatminigame、minigame、报错]
- 咨询: 在 Creator 3.4.0 版本中，构建微信小游戏和字节小游戏运行报错(TypeError: Cannot read property 'prototype' of undefined)

A: 项目中脚本存在循环引用，导致基类找不到产生的报错。

### 16 [小游戏、minigame、阴影]
- 咨询: 在 Creator 3.x 版本中，小游戏内阴影显示异常(阴影变成小黑圆圈)

A: 3D 节点上可能存在 UITransform 和 Widget 组件(这适用于 UI 的组件)导致的异常，移除 UI 组件后重试。

### 17 [小游戏、minigame、音频]
- 咨询: 在 Creator 2.0.10 版本中，动态加载的音效播放没有声音。CocosCreator 2.0.10, 使用`cc.loader.loadRes`加载的 audioClip 在部分手机上没有声音，比如 RedMI K30s Ultra上, 查看加载之后的 Audio 属性，一些参数是 undefined。出问题的音频有提示过“音频文件已损坏”, 使用播放器播放是可以的, 怀疑是之前用 apple music 剪辑后压缩导致的。    

A: 可以尝试修改音频的采样率为`44.1KHZ`。因为在当前的主流采集卡上，音频的采样频率一般共分为 22.05KHZ、 44.1KHZ、48KHZ 三个等级, 其中 22.05KHZ 只能达到 FM 广播的声音品质, 44.1KHZ 则是理论上的 CD 音质界限, 48KHZ 则更加精确一些, 高于 48KHZ 的采样频率人耳就无法分辨出来了。 另外可以先用原本没有处理过的原音频播放试试, 可以排除剪辑压缩导致的问题。

### 18 [小游戏、微信小游戏、wechatminigame、minigame、报错]
- 咨询: 在 Creator 3.1.2 版本中，使用es6语法发布微信小游戏报错'Unhandled promise rejection ReferenceError: regeneratorRuntime is not defined'。CocosCreator 3.1.2，使用到了同步执行的Promise，报错 'Unhandled promise rejection ReferenceError: regeneratorRuntime is not defined'    

A: 首先确认打开自动转 ES5 微信开发者工具->本地设置->将JS编译成ES5 的选项需要勾选。其次需要确保脚本文件小于`500k`，因为超过500k 的脚本文件无法转换成 ES5。最后需要打开增加编译 微信开发者工具->本地设置->增加编译。

### 19 [小游戏、微信小游戏、wechatminigame、minigame、报错]
- 咨询: 在 Creator 3.4.0 版本中，微信小游戏报错 'TypeError: A.__uid is not a function at Object.texSubImage2D (magicbrush.js:1:17083)'。CocosCreator 3.4.0 打包微信小游戏报错，之前3.3.2版本以及之前的版本没有报错，并且游戏会闪退。    

A: 检查一下是不是因为在微信小游戏里使用了子域导致的问题(隐藏子域的节点看是否运行正常)，其次检查子域获取的传值是否正确，是否在新版本里面使用的是之前生成的子域模板，如果是的话，需要重新生成子域模板。另外需要在构建微信小游戏的时候，勾选`生成开发数据与工程模板`选项。

### 20 [小游戏、minigame、广告]
- 咨询: 视频广告结束后触发了断线重连，导致奖励无法正常发放？看完视频广告之后关闭广告页面以后向服务器发送发放视频奖励协议，但是触发了onClose导致无法收到服务器返回的消息, 触发onClose会执行断线重连逻辑。    

A: 这个问题就需要自行在断线重连之后查询服务器，是否存在奖励未领取的情况。

### 21 [安卓、android、报错]
- 咨询: 在 Creator 2.4.6 版本中，bugly捕获奔溃异常 'E AndroidRuntime: java.lang.NullPointerException: Attempt to invoke virtual method "java.lang.String de.a()" on a null object reference'，在 bugly 奔溃检测上以上信息，看似刚启动就奔溃了，是否是已知问题，而且不是必现问题。
- BUG 信息: E AndroidRuntime: java.lang.NullPointerException: Attempt to invoke virtual method "java.lang.String de.a()" on a null object reference      
- Detail 信息:
```
 ... 
W System : ClassLoader reference unknown path : system/framework/mediatek-cta.jar
 ...
D AndroidRuntime: Shutting down VM
-------- beginning of crash
E AndroidRuntime : FATAL EXCEPTION : main
...
E AndroidRuntime: java.lang.NullPointerException: Attempt to invoke virtual method 'java.lang.String de.a()' on a null object reference
...
E AndroidRuntime : at com.http.lib.request.Request$c$g.run(Request.java:1)
...
```
```
 ... 
W System : ClassLoader reference unknown path : system/framework/mediatek-cta.jar
 ...
D OpenSSLLib: OpensslErr:Module:12(177:); file:External/boringssl/src/crypto/asn1/asn1_lib.c; Line: 168; Function: ASN1_get_object
 ...
D AndroidRuntime: Shutting down VM
-------- beginning of crash
E AndroidRuntime : FATAL EXCEPTION : main
...
E AndroidRuntime: java.lang.NullPointerException: Attempt to invoke virtual method 'java.lang.String de.a()' on a null object reference
...
E AndroidRuntime : at com.http.lib.request.Request$c$g.run(Request.java:1)
...
```

A: 不是已知问题，没有收到类似反馈。前面看到有 jar 包丢失了，排查是不是 dex 分包导致的，有没有使用到 dex 分包，或者剔除了相关的东西。另外如果构建 API 为30或者31的之前有类似反馈，会出问题，可以考虑降低API看看 https://forum.cocos.org/t/topic/122966

### 22 [安卓、android、下载]
- 咨询: 下载apk如何操作? 需要从远程CDN服务器下载apk包并安装打开.   

A: 引擎目前没有提供".apk"格式的文件下载, 可以自行扩展。另外如果远程CDN服务器上下载的apk包过大，可能会导致使用assetManager.loadAny下载接口下载超时，可以修改CocosDownloader.java脚本中的OkHttpClient.class中createDownloader接口中的 downloader._httpClient 的 callTimeout改为pingInterval 模式。另外可以通过java层下载apk，可以参考 https://blog.csdn.net/ly_xiamu/article/details/83089534。

### 23 [安卓、android]
- 咨询: 在 Creator 3.4.0 版本中，打包安卓后透明材质无法显示?

A: 打包安卓，是支持透明材质的，透明物体是按照顺序渲染的，透明材质无法显示的问题，怀疑是被遮挡了。检查一下你这个材质是否勾选了深度检测。

### 24 [苹果、iOS]
- 咨询: 在 Creator 2.0.10 版本中，JSB 自动绑定, C++ 代码失效? iOS平台, 写了两个C++代码的类, 也使用了`#include`了, 但是jsb调用的时候，提示类没有定义。

A: 查看编译有没有报错, 是否是代码写的问题。查看实际编译是否生效, 直接在xcode上调试代码即可。比如 register_all_spine 是一个注册回调函数，你加个断点看看，是否有进度断点。另外，如果是 #include 引用是 加在Classes/jsb_module_register.cpp 里面, 需要修改加到 manual/jsb_module_register.cpp 里面。因为 在Classes下的文件每次构建都会被覆盖。

### 25 [苹果、iOS、报错]
- 咨询: 在 Creator 2.4.7 版本中，提示"不支持的 URL"。iOS发送字母和数字没有问题，发送中文就会有 "Response failed, error buffer: 不支持的URL" 提示。 

A: iOS平台需要自行做下 urlEncode 操作，不然URL的中文会解析异常。

### 26 [苹果、iOS、报错]
- 咨询: 在 Creator 2.4.x 版本中，设置 pvr 并勾选 genMipmaps 运行报错? 设置 PVR 并勾选 Gen Mipmaps，构建 web 平台，在 iOS 上运行报错。    

A: pvr 不支持 gl.generateMipmap, pvr 的 mipmap 是用工具离线生成的，保存在 pvr 文件里面的。Creator 编辑器目前无法为 pvr 生成 mipmap，需要自行二次处理一下图片资源。 @link https://programmerclick.com/article/6628594252/

### 27 [苹果、iOS]
- 咨询: 在 Creator 3.5.0 版本中，Xcode 中导入 framework 库不成功？

A: 这是引擎的 cmake 模板有问题导致的 BUG。用户设置的 CC_UI_RESOURCES 没有使用被合并导致的。可以使用 set(CC_UI_RESOURCES, ${CC_UI_RESOURCES} xxx) 处理。该问题会在 3.5.1 版本上解决。    
修复的 PR https://github.com/cocos/cocos-engine/pull/11011/files    
cmake 模板的位置    
- Windows: D:\CocosDashboard\resources\.editors\Creator\3.5.0\resources\resources\3d\engine-native\templates\cmake
- macOS: /Applications/CocosCreator/Creator/3.5.0/CocosCreator.app/Contents/Resources/resources/3d/engine/native/templates/cmake

### 28 [web、浏览器、本地数据]
- 咨询: 如何写入数据到文件? 为了开发测试写一些测试数据，需要吧数据写入到某个 txt 文本中。  
       
A: 原生平台可以使用`jsb.FileUtils.writeStringToFile`写入数据。Web 平台没有办法访问文件系统，这个是浏览器限制没办法突破。可以考虑使用本地缓存`sys.localStorage`。

### 29 [web、浏览器、构建发布]
- 咨询: Cocos Creator为什么发布web后，空工程也会CPU占用比较高?

A: 浏览器屏幕刷新本身就会有开销的，虽然什么都不做，但实际上也是 60 fps 满帧在跑。建议试试发布成安卓原生，应该开销会小一些。 @jare https://forum.cocos.org/t/topic/126233/2

### 30 [web、浏览器、报错]
- 咨询: Cocos Creator 3.3.2 升级到 3.4.0社区版报错提示(Error: UI element #step-length dosen't exist.)

A: Web上预览时报错这个提示，是因为项目运行时使用了运行时查看节点树的插件导致的。这个插件可能暂时不兼容3.4.0版本，所以异常报错。处理方式是删除这个插件即可。

### 31 [web、浏览器]
- 咨询: 在 Creator 2.4.6 版本中，如何修改 web 的 vconsole? 打包 Cocos Creator 2.4.6 版本，需要修改 vconsole 按钮的位置。

A: 修改 vconsole.min.js 文件，可以搜索 __vconsole .vc-switch
```
#__vconsole .vc-switch {margin-bottom: 400px; display:block ... }
```
或者在 main.js 脚本动态修改该 CSS 的 style
```
var vc_switch = document.getElementsByClassName('vc-switch');
setInterval(() => { vc_switch[0].style.width = '100%';}, 100)
```

### 32 [web、浏览器]
- 咨询: 如何去除加载页?
A:    

2.x 版本   
```ts
1.1 构建 web 完成之后，修改 index.html 中的 L19 splash.style.display
> splash.style.display = 'none';

1.2 修改 main.xxx.js 中的 L19 splash.style.display
> splash.style.display = 'none';

1.3 修改 style-mobile.xxx.css 和 style-desktop.xxx.css，新增
>#GameCanvas {
    width: 0%;
    height: 0%;
}
```
3.x版本
```ts
构建 Web 版本的时候, 替换插屏 功能勾选, 设置显示时间为 0
```

### 33 [编辑器、editor、属性定义]
- 咨询: 在属性面板如何定义二维数组?

A:    
2.x版本
```ts
const {ccclass, property} = cc._decorator;

@ccclass('test')
export class test {
    @property({type: cc.String})
    t0: string[] = [];
    @property({type: cc.Float})
    t1: number[] = [];
    @property({type: cc.Node})
    t2: cc.Node[] = [];
}
@ccclass
export class Main extends cc.Component {
    @property(test)
    a: test[] = [];
}
```

3.x版本
```ts
import { _decorator, Component, CCFloat } from 'cc';
const { ccclass, property } = _decorator;

@ccclass('TestArray')
export class TestArray {
    @property({type: CCFloat })
    t0 : number[] = [];
}
@ccclass('Test')
export class Test extends Component {
    @property(TestArray)
    a : TestArray[] = [];
}
```

### 34 [编辑器、editor、报错]
- 咨询: 在 Creator 3.4.0 版本中，构建报错([Build]Cannot read property 'type' of undefined @getDependUuidsDeep xxx)

A: 构建的时候，项目中依赖的资源丢失导致的报错。

### 35 [编辑器、editor]
- 咨询: 在 Creator 2.x 版本中，Setting 文件中的 packedAssets 的生成规则？

A: packedAssets 是用来描述合并后 JSON 的，key 值是合并后生成的 JSON 的文件名，value 值是被合并的资源的 key 值。

### 36 [编辑器、editor]
- 咨询: 在 Creator 构建后资源压缩比例？

A: 特效图片可以提高压缩率（60至70），模型可以使用压缩率（70至80），UI 合图和字体合图可以使用高质量压缩（90）。 @link https://forum.cocos.org/t/topic/76554

### 37 [编辑器、editor]
- 咨询: Cocos Creator 2.x 关闭资源刷新? 项目比较大的时候，刷新资源要耗费很长的事件，想要关闭资源的刷新。    

A: 2.x 的项目，可以修改 Creator 本地缓存文件中的本地刷新配置来关闭资源刷新。如 Windows 系统，修改 C://Users/Administrator/.CocosCreator/profiles/settings.json 中的 "watch-file" 参数，修改为 false 就不会自动刷新资源。另外刷新资源，可以在插件中使用 Editor.assetdb.refresh 来刷新某个资源。

### 38 [编辑器、editor]
- 咨询: 在 Creator 3.x 版本中，同一个项目构建不同的安卓包。需要在同一个项目，打包不同的安卓包因为我们可能需要根据不同的渠道对接不同的sdk，但是目前直接构建只有一个 native 文件夹，如何实现同一个项目构建不同的 native 文件夹呢?    

A: 参考官方文档 https://docs.cocos.com/creator/manual/zh/editor/publish/custom-build-plugin.html 扩展构建流程, 文件的拷贝需要自行拓展，构建扩展包可以通过菜单栏 -> 扩展 -> 创建扩展 快速生成扩展空模板。

### 39 [编辑器、editor、导入项目]
- 咨询: Dashboard 导入 2.0.10 项目失败

A: 修改项目下的project.json (小于 Cocos Creator 2.3.2 的版本都适用)
```
{
    "engine": "cocos-creator-js",
    "type": "2d",
    "version": "2.0.10",
    "packages": "packages"
}
```

### 40 [编辑器、editor、压缩纹理]
- 咨询: 在 Creator 3.x 版本中，压缩纹理配置里面的百分数的含义。压缩纹理配置里面的百分数的含义， 如果配置 JPG 和 PNG 则显示异常(发黑)，单独配置 PNG 则显示正常

A: 压缩纹理配置里面的百分数代表压缩到圆度的百分比。如果同时配置 JPG 和 PNG，则优先加载的是 JPG，而不是 PNG。
```
[压缩纹理详解](https://docs.cocos.com/creator/manual/zh/asset/compress-texture.html#%E5%8E%8B%E7%BC%A9%E7%BA%B9%E7%90%86%E8%AF%A6%E8%A7%A3)
Cocos Creator 3.0 在构建图片的时候，会查找当前图片是否进行了压缩纹理的配置，如果没有，则最后按原图输出。

如果查找到了压缩纹理的配置，那么会按照找到的配置对图片进行纹理压缩。项目设置里压缩纹理配置是按照平台大类划分的，具体到实际平台的支持程度会有一些差异。构建将会根据 实际构建平台 以及当前 图片纹理的透明通道 情况来对配置的纹理格式做一定的剔除和优先级选择，关于这块规则可以参考下文的示例来理解。

这些生成的图片不会都被加载到引擎中，引擎会根据 macro.SUPPORT_TEXTURE_FORMATS 中的配置来选择加载合适格式的图片。macro.SUPPORT_TEXTURE_FORMATS 列举了当前平台支持的所有图片格式，引擎加载图片时会从生成的图片中找到在这个列表中 优先级靠前（即排列靠前）的格式来加载。

开发者可以通过修改 macro.SUPPORT_TEXTURE_FORMATS 来自定义平台的图片资源支持情况以及加载顺序的优先级。
```
而从 [macro.SUPPORT_TEXTURE_FORMATS](https://github.com/cocos-creator/engine/blob/v3.4.0/cocos/core/platform/macro.ts#L35) 所有平台支持的格式列表 ['.webp', '.jpg', '.jpeg', '.bmp', '.png'] 可以看出，如果同时配置JPG和PNG，JPG是优先被选择作为加载的格式的。
```
const SUPPORT_TEXTURE_FORMATS = ['.astc', '.pkm', '.pvr', '.webp', '.jpg', '.jpeg', '.bmp', '.png'];
```

### 41 [编辑器、editor]
- 咨询: Cocos Creator 3.x 图片无法拖到层级管理器上显示?

A: 点击资源管理器中的图片，选择图片的 TYPE 为 sprite-frame。

### 42 [编辑器、editor]
- 咨询: 在 Creator 3.x 版本中，如何快速定位模块中未勾选但已在项目中使用的模块?

A: 目前未勾选但是被剔除的功能模块，在运行时会提示 xxx is undefined 报错。另外，可以通过构建 web 版本后的 cc.js 内容来分辨引擎模块(这个你需要比较熟悉哪些模块对应哪些代码) 或者 settings/project.json/excluded-modules 来区分你的模块。

### 43 [编辑器、editor]
- 咨询: 在 Creator 2.4.6 版本中，config.json 内的 uuids 和 paths 不对应? 在需要对处理 debug 和 release 模式的功能限制时，发现 config.json 中，一些 uuid 在 path 中找不到。而且 release 模式下 path 的 key 值也可能不相邻。    

A: path 的列表里是所有资源的路径，但是这个资源可能包括 texture、spriteFrame 等，然后有些 uuid 存在于一个比较大的资源下，比如资源挂载到场景下，这个时候，需要在场景资源里面才能找到该 uuid 的资源。换句话说，path 是所有文件的信息，uuid 包括了精灵帧等，所以会对应不连续，会比 path 的条目多。

### 44 [编辑器、editor]
- 咨询: Cocos Creator 2.2.2 怎么判断非 resources 文件夹的资源是否属于公共资源？

A: 目前引擎（2.2.2）没有办法在运行时将 res 的包体资源相对路径转化为项目相对路径。可以参考：
  - 方式1 通过监听 cc.loader.onProgress = function (completedCount, totalCount, item) {}; 可以获取到每次加载的实例。或者在加载完成之后通过 cc.loader.getDependsRecursively("resUrl") 获取实例。取得实例之后缓存他们的路径，然后可以通过 cc.loader.getRes(url, cc.Asset) 获取到原始资源对象。此时就可以通过解读原始对象数据，判断是不是公用资源。
  - 方式2 通过插件获取项目发布之后的资源路径，然后将该路径数据整理出来，记录成 json，放到项目中。然后你每次需要卸载资源的时候，通过第一步获取到的路径，然后去比对，如果路径已经被记录在公有资源 json 中，那么就可以卸载。
  - 参考源码
  > CCLoader 源码 https://github.com/cocos-creator/engine/blob/9b7a7dc11ce49f0fdca3c34df5ab59604060c0a4/cocos2d/core/load-pipeline/CCLoader.js#L102
  >
  > CCLoader API https://docs.cocos.com/creator/api/zh/classes/loader.html#assetloader
  >
  > 项目导出后的路径可以通过这个插件来查看 https://forum.cocos.org/t/topic/76554
  >
  > 官方插件 https://github.com/cocos-creator/demo-process-build-textures

### 45 [编辑器、editor]
- 咨询: Cocos Creator 有没有获取分包下所有文件路径的方法?

A: 目前引擎没有提供这种方法，可以自己封装。获取 Bundle 之后，获取 config/paths 下的 _map 数组。

### 46 [小游戏、微信小游戏、wechatminigame、minigame、报错]
- 咨询: Creator 1.9.1 版本，微信小游戏子域使用艺术字字体报错？

A: 微信小游戏子域可以使用艺术字，但是艺术字只能是白色。https://forum.cocos.org/t/topic/60785/3 

### 47 [小游戏、微信小游戏、wechatminigame、minigame]
- 咨询: 微信小游戏子域工程的资源如何像主域工程远程加载？子域工程也有不少图片资源，很容易构建后超过微信小游戏限制 4MB。

A: 微信小游戏限制子域分包，不支持吧临时文件保存在子域 res 文件夹，子域也不能访问主域的 res 文件夹。https://developers.weixin.qq.com/community/develop/doc/00048689504138cd3007e49ea56800 。 开放域的 Image 只能使用本地或微信 CDN 的图片，不能使用开发者自己服务器上的图片。对于非本地货非微信 CDN 的图片，可以先从主域 wx.downloadFile() 下载图片文件，再通过 OpenDataContext.postMessage() 把文件路径传给开放域使用。https://forum.cocos.org/t/res/63123/5

### 48 [编辑器、editor]
- 咨询: 编辑器双屏问题导致编辑器跑到屏幕之外？

A: 删除项目中的 local 文件夹的 layout.windows.json 文件。https://forum.cocos.org/t/topic/66870/5

### 49 [小游戏、minigame、oppo、vivo、报错]
- 咨询: 在 Creator 2.0.5 版本, 构建`rpk`失败，报错`rpk包构建失败！错误：Error: Command failed: quickgame.cmd pack`、`Error:release 签名不存在!`。

A: 报错`rpk包构建失败！错误：Error: Command failed: quickgame.cmd pack`是没有正确配置快游戏环境导致，可以将快游戏路径，如`E:quickgame-toolkit\lib\bin`目录路径加入环境变量 Path 中, 保存之后，可以使用 `node "E:quickgame-toolkit\lib\bin\index.js" -v` 命令查看配置，看到 quickgame-toolkit 版本，解决问题。https://forum.cocos.org/t/oppo-vivo-bug/68839/11

### 50 [四元数]
- 咨询: 在 Creator 2.1.0 版本中，`rotationY 被警告 DEPRECATE`，如何处理？

A: 可以自定义引擎，在`engine/CCNode.js`的`rotateY`的**set**接口，在
```ts
this._renderFlag != RenderFlow.FLAG_TRANSFORM;
this._fromEuler();
...
```
或者通过设置`eulerAngles`，参数类型为 Vec3, `node.eulerAngles = cc.v3(x,y,z);`  https://forum.cocos.org/t/v2-1-0/70818/6

### 51 [Label]
- 咨询: Label 在下一帧才会刷新文本大小？

A: 由于 label 中的 _updateRenderData 处理开销过大，导致没办法设置 label 的 string 文本没及时触发更新，label 的 size 只能在渲染时才能获取到正确的 size。可以在设置文本内容后，通过`label.updateRenderData(true);`接口去动态更新 label 的 size。 https://forum.cocos.org/t/topic/65851/4

### 52 [热更新、hotupdate、编辑器]
- 咨询: 在 Creator 2.0.x 版本，使用热更新报错？

A: creator 2.0，第一次加载的资源是原生的，而不是更新后的资源，执行热更新流程后，才会把manifest的热更新目录加入到文件的search path，所以完成热更新并执行更新后的资源，你需要两个步骤，1. 执行热更新，2.不管热更新成功与否，执行 cc.game.restart(); 重新加载游戏，示例代码中的ALREADY_UP_TO_DATE中没有重启游戏，导致热更新完成后，再次打开不会切换到更新后的界面。第二个问题，不要使用官方demo下面的热更新资源，因为脚本的加密密匙匹配不上，自己根据教程，重新导出热更新资源。https://forum.cocos.org/t/topic/68452/16    
https://docs.cocos.com/creator/2.4/manual/zh/advanced-topics/assets-manager.html
https://docs.cocos.com/creator/2.4/manual/zh/advanced-topics/hot-update.html

### 53 [子域]
- 咨询: 在 Creator 2.0.x 版本，子域 ScrollView 无法滚动？

A: https://forum.cocos.org/t/scrollview/67114/15?u=337031709

### 54 [web、浏览器、调式]
- 咨询: 在 Creator web 预览时，如何不显示调式信息？

A: Web 预览时，点击预览窗口的 ShowFPS 可以关闭调式信息，或者在代码中设置`setDisplayStats(false);`https://forum.cocos.org/t/cocoscreator/70236/2?u=337031709
```ts
// 低版本 2.x
cc.debug.setDisplayStats(false);
// 高版本 3.x
cc.profiler.setDisplayStats(false);
```

### 55 [构建、音频、报错]
- 咨询: 在 Creator 1.9.1 版本中，构建 web 后，预加载音频或者播放背景音乐报错。

A: 排查构建模版是否异常或者模块剔除是否把`audio`剔除，导致问题。https://forum.cocos.org/t/topic/70759/2

### 56 [vscode]
- 咨询: vscode 升级后 Creator 添加编译任务后使用出错。

A: 这个编译任务因为vscode的升级编译任务系统引起的问题，https://code.visualstudio.com/docs/editor/tasks#vscode3 目前的解决方案是先将原先的tasks.json文件编码修改为如下，同时按住ctrl + shift + b 运行任务，先解决问题。https://forum.cocos.org/t/vscode/70253/3
```json 
{
    "version": "2.0.0",
    "tasks": [
        {
            "label": "compile",
            "command": "curl",
            "args": ["http://localhost:7456/update-db"],
            "type":"shell",
            "isBackground": true,
            "group": "build",
            "presentation": {
                // Reveal the output only if unrecognized errors occur.
                "reveal": "always"
            },
        }
    ]
}
```

### 57 [编辑器、editor，报错]
- 咨询: 在 Creator 2.0.2 版本，场景打不开，报错`TypeError: Cannot read property "_id" of null`。

A: 右键场景文件`.scene`使用**还原到上次保存**功能试试。如果有保存或托管了场景文件，请对比下文件差异，这个问题应该是异常保存了场景文件导致的场景文件某个属性出现异常。用vscode 等代码编辑器打开异常项目，查看下_id是否为null。https://forum.cocos.org/t/cc-bug/67971/16
https://forum.cocos.org/t/topic/70088/2