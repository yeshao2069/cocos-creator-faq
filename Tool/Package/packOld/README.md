## Cocos Creator FAQ

### Package Download Link / 安装包下载地址(在线地址)

#### 使用方式： 如果直接店家下载无效的话(浏览器访问被限制)，可以复制下载链接到浏览器新的 TAB 打开，然后即可下载。

#### Cocos Creator v0.7 && Cocos Creator 3D 1.x
| 文件名 | 下载链接 | 百度网盘 |
| :---: | :---: | :---: |
| CocosCreator_v0.7.1-win | http://cocostudio.download.appget.cn/CocosCreator/v0.7.1/CocosCreator_v0.7.1_win_en.zip | 链接: https://pan.baidu.com/s/1HyW6tNbW-8AGI3QOzQDGHw 提取码: 9mue |
| CocosCreator_v0.7.1-mac | http://cocostudio.download.appget.cn/CocosCreator/v0.7.1/CocosCreator_v0.7.1_mac_en.zip | 链接: https://pan.baidu.com/s/12tjR9Bff-GIq5BwL1Cgfqw 提取码: ltol |
| CocosCreator_v0.7.0-win | http://cocostudio.download.appget.cn/CocosCreator/v0.7.0/CocosCreator_v0.7.0_win.zip | 链接: https://pan.baidu.com/s/1Lj3G_x1p9dmFnvtlVpK43g 提取码: 29m6 |
| CocosCreator_v0.7.0-mac | http://cocostudio.download.appget.cn/CocosCreator/v0.7.0/CocosCreator_v0.7.0_mac.zip | 链接: https://pan.baidu.com/s/18VQhS2A2HE8hMGNaTeCvxQ 提取码: ej3h |
| CocosCreator3D-v1.0.0-win | http://www.cocos2d-x.org/filedown/CocosCreator3D-v1.0.0-win | 链接: https://pan.baidu.com/s/18p5hUoVO3ArBXpS7jITeYw 提取码: s5fq |
| CocosCreator3D-v1.0.0-mac | http://www.cocos2d-x.org/filedown/CocosCreator3D-v1.0.0-win | 链接: https://pan.baidu.com/s/1ADhdZ8TVxuccY0TV3UmXvg 提取码: qlv3 |
| CocosCreator3D-v1.0.1-win | http://www.cocos2d-x.org/filedown/CocosCreator3D-v1.0.1-win | 链接: https://pan.baidu.com/s/1JWxBypt6KLxAKI-z3nTvwg 提取码: 8jwl |
| CocosCreator3D-v1.0.1-mac | http://www.cocos2d-x.org/filedown/CocosCreator3D-v1.0.1-win | 链接: https://pan.baidu.com/s/18rNbXlUu4a3s_Wfodezrwg 提取码: 9af3 |
| CocosCreator3D-v1.0.2-win | http://www.cocos2d-x.org/filedown/CocosCreator3D-v1.0.2-win | 链接: https://pan.baidu.com/s/1k22SjlzYzX_P9Md-_tt-SQ 提取码: akef |
| CocosCreator3D-v1.0.2-mac | http://www.cocos2d-x.org/filedown/CocosCreator3D-v1.0.2-mac | 链接: https://pan.baidu.com/s/1c0eAa7lcntmun99ClXoxgQ 提取码: t03v |
| CocosCreator3D-v1.0.3-win | https://cocos2d-x.org/filedown/CocosCreator3D_v1.0.3_win | 链接: https://pan.baidu.com/s/120DvNrNw2p8TVD_DIGUVqg 提取码: aeeo |
| CocosCreator3D-v1.0.3-mac | https://cocos2d-x.org/filedown/CocosCreator3D_v1.0.3_mac | 链接: https://pan.baidu.com/s/18ZIKOrTrCjxdkuNce9k1sw 提取码: qw8b |
| CocosCreator3D-v1.0.4-win | https://cocos2d-x.org/filedown/CocosCreator3D-v1.0.4_win | 链接: https://pan.baidu.com/s/1QpuVY04j5KL9MNKOZZLI9g 提取码: ucw6 |
| CocosCreator3D-v1.0.4-mac | https://cocos2d-x.org/filedown/CocosCreator3D-v1.0.4_mac | 链接: https://pan.baidu.com/s/1zYvpsyXgtP3YoUTjLeJc6Q 提取码: wv46 |
| CocosCreator3D-v1.1.0-win | https://download.cocos.com/Cocos3D/v1.1.0/CocosCreator3D-v1.1.0-win32-050622.zip | 链接: https://pan.baidu.com/s/1_f5i9b7GNAQ4tiIcHgLQVw 提取码: tmsp |
| CocosCreator3D-v1.1.0-mac | https://download.cocos.com/Cocos3D/v1.1.0/CocosCreator3D-v1.1.0-darwin-050622.zip | 链接: https://pan.baidu.com/s/11sqCce632Igek1IqoUveeg 提取码: 3i4w |
| CocosCreator3D-v1.1.1-win | https://download.cocos.com/Cocos3D/v1.1.1/CocosCreator3D-v1.1.1-win32-070119.zip | 链接: https://pan.baidu.com/s/1i8nJlM5-F7UiWqYFeSBskw 提取码: hqzf |
| CocosCreator3D-v1.1.1-mac | https://download.cocos.com/Cocos3D/v1.1.1/CocosCreator3D-v1.1.1-darwin-070118.zip | 链接: https://pan.baidu.com/s/13VMFVCk-1W97vHv10wGhMQ 提取码: fqxm |
| CocosCreator3D-v1.1.2-win | https://download.cocos.com/Cocos3D/v1.1.2/CocosCreator3D-v1.1.2-win32-080622.zip | 链接: https://pan.baidu.com/s/1Y8gqYW5anjPMeA6yx8Qy2g 提取码: 5jvc |
| CocosCreator3D-v1.1.2-mac | https://download.cocos.com/Cocos3D/v1.1.2/CocosCreator3D-v1.1.2-darwin-080622.zip | 链接: https://pan.baidu.com/s/1t1Kby0Jj6AXsoPQ4_WdTXQ 提取码: n5vq |
| CocosCreator3D-v1.2.0-win | https://download.cocos.com/Cocos3D/v1.2.0/CocosCreator3D-v1.2.0-win32-092813.zip | 链接: https://pan.baidu.com/s/1XQJ6-_dnMo6bcTjA-NR7SA 提取码: yigy |
| CocosCreator3D-v1.2.0-mac | https://download.cocos.com/Cocos3D/v1.2.0/CocosCreator3D-v1.2.0-darwin-092813.zip | 链接: https://pan.baidu.com/s/1uk4id_fMrhSy_OT-sevPBw 提取码: 82bb |