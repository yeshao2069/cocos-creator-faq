## Cocos Creator FAQ

### Package Download Link / 安装包下载地址(在线地址)

### DragonBones 5.x 编辑器下载
| 文件名 | 下载链接 | 百度网盘 |
| :---: | :---: | :---: |
| DragonBonesPro-v5.6.3_win | / | 链接: https://pan.baidu.com/s/1vc7t-Ug4w-bxTJSule1hxw?pwd=mkcf 提取码: mkcf |
| DragonBonesPro-v5.1.0_win | / | 链接: https://pan.baidu.com/s/1cxVnjnL-APIIZwsRI43XSQ?pwd=9c7t 提取码: 9c7t |