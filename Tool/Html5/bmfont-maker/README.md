
### BMFont Maker
BMFont 制作器

- 使用方式
* 点击`index.html`，打开界面。将`BMFont`的散图资源拖入页面的方框中。
* 在`导出文件名字`中输入需要输出的`BMFont`文本的资源名。
* 点击`save`可以保存并导出。
* 点击`clear`进行清除。

- 原文链接
https://forum.cocos.org/t/topic/120297