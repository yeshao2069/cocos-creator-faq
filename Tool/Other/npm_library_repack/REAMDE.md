

### NPM 库重新打包

1. 在 `plugin/use.js` 中写入需要调用的 NPM 库的使用代码

2. 在项目下，执行 `npm install` 进行安装

3. 在 `assets/plugins` 路径下会生成 repack.js 脚本，将 repack.js 重新命名，导入实际项目，设置为插件，设置范围为 global (全局)，并允许在 web 上加载