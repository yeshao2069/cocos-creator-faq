import { NavMesh } from "nav2d";
window.NavMesh = NavMesh;
const navmesh = new NavMesh([
[
    [0, 0],
    [0, 12],
    [12, 0],
],
[
    [12, 8],
    [12, 4],
    [16, 6],
],
[
    [12, 0],
    [6, 6],
    [12, 6],
],
[
    [100, 100],
    [110, 100],
    [100, 110],
    [95, 107],
    [105, 102],
],
]);

const path = navmesh.findPath([1, 1], [14, 6]);
console.log(path);