const path = require('path');

module.exports = {
  entry: './plugin/use.js',
  output: {
    path: path.resolve(__dirname, 'assets/plugins/'),
    filename: 'repack.js'
  },
  module: {
    rules: [
      {
        test: /\.js$/,
        use: {
          loader: "babel-loader"
        }
      }
    ]
  }
};
