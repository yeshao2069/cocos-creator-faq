## 图片去除黑边 & 图片压缩 工具

### 使用方式
- 无需搭建环境, 双击打开就可以使用
- 在双击后或者命令行运行后，拖入单张图片或者文件夹即可，会将原图或者文件夹下的图片去除黑边并压缩
- 通过 config 文件配置 tinypng 的 token, 开启图片压缩的功能. token 可以在 https://tinify.com/dashboard/api 获取

### 相关链接
https://forum.cocos.org/t/topic/124355