import os
from string import Template
import platform
import json

global config

build_args_array = []

def load_json(file_name):
    with open(file_name, "r") as f:
        data = json.load(f)
    return data

if __name__ == '__main__':
    global config

    scriptRoot = os.path.split(os.path.realpath(__file__))[0]
    os.chdir(scriptRoot)

    config = load_json("config.json")

    creator_exe_path = config["creator_exe_path"]
    os.chdir(creator_exe_path)

    project_path = config["project_path"]
    platform = config["platform"]
    build_path = config["buildPath"]

    print("----------------------开始构建-------------")

    build_args = 'platform=' + platform + ";buildPath=" + build_path
    os.system('CocosCreator.exe --path %s --build "%s"' %(project_path, build_args))

    print("----------------------构建完成-------------")

    print("----------------------开始编译-------------")

    compile_args = 'platform=' + platform
    os.system('CocosCreator.exe --path %s --compile "%s"' %(project_path, compile_args))

    print("----------------------编译完成-------------")

    file_generate_path = project_path + "/" + build_path + '/jsb-link/publish'

    print("编译构建后的" + platform + "路径:" + file_generate_path)