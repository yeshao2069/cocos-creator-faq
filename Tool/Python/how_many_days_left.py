# -*- coding: utf-8 -*-
# author: Muxiandong  
# date: 2023.2.22

from datetime import date, timedelta
import random


minyan = [
    "如果你不能飞，那就跑；\n如果跑不动，那就走；\n实在走不了，那就爬，无论做什么，你都要勇往直前。\n\t\t\t\t\t\t\t--马丁·路德·金",
    "世上无难事，只畏有心人。\n有心之人，即立志之坚午也，志坚则不畏事之不成。\n\t\t\t\t\t\t\t--任弼时",
    "静以修生，俭以养德，非淡泊无以明志，非宁静无以致远。\n\t\t\t\t\t\t\t--诸葛亮",
    "有远大志向而脚踏实地，有障碍失败而不言放弃，这样坚持下去，不会成功也会获益无穷。\n\t\t\t\t\t\t\t--方海权",
    "事业常成于坚忍，毁于急躁。\n\t\t\t\t\t\t\t--萨迪",
    "不放弃！决不放弃！永不放弃。\n\t\t\t\t\t\t\t--邱吉尔",
    "骆驼走得慢，但终能走到目的地。\n\t\t\t\t\t\t\t--张林权",
    "涓滴之水终可磨损大石，不是由于它的力量强大，而是由于昼夜不舍的滴坠。\n\t\t\t\t\t\t\t--贝多芬",
    "穷且益坚，不坠青云之志。\n\t\t\t\t\t\t\t--王勃"
]


if __name__ == '__main__':

    today = date.today()
    # print(today)

    dates = str(today).split("-")

    year = int(dates[0])
    month = int(dates[1])
    day = int(dates[2])
    Sum = 0  
    total_month = (31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31)
    Sum = sum(total_month[:month - 1]) + day

    # percent = (Sum / 365) * 100
    left = 365 - Sum

    percent = Sum * 100 / 365

    if year % 400 == 0 or (year % 4 == 0 and year % 100 != 0): 
        if month > 2:  
            Sum += 1  
        # percent = (Sum / 366) * 100  
        left = 366 - Sum

        percent = Sum * 100 / 366
    
    percent = round(percent, 2)

    index = random.randint(0, len(minyan) - 1)
    my = minyan[index]

    print("")
    print("")
    print("*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*")
    print("")
    print("尊敬的木先生: ")
    print("今天是 " + str(today))
    print("{0}年{1}月{2}日是{3}年的第 {4} 天, 今年就只剩下 {5} 天, 还剩下 {6} %！".format(year, month, day, year, Sum, left, 100 - percent))
    print("")
    print("") 
    print(my)
    print("") 
    print("*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*")
    print("")
    print("")