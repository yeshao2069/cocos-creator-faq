import base64

# Base64字符集（包含填充符=）
BASE64_KEYS = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/="
HexChars = list("0123456789abcdef")

# 预计算Base64字符的索引值
BASE64_VALUES = {ord(c): i for i, c in enumerate(BASE64_KEYS)}
HexMap = {c: i for i, c in enumerate(HexChars)}

# UUID模板结构 [9](@ref)
UuidTemplate = list("00000000-0000-0000-0000-000000000000")
Indices = [i for i, c in enumerate(UuidTemplate) if c != '-']

def decode_res_uuid(compressed):
    """ 解压22字符的Base64 UUID为36字符标准格式 [9,10](@ref)"""
    if len(compressed) != 22:
        return compressed
    
    uuid_part = compressed.split('@')[0]
    result = UuidTemplate.copy()
    result[0] = uuid_part[0]
    result[1] = uuid_part[1]
    
    j = 2
    for i in range(2, 22, 2):
        lhs = BASE64_VALUES[ord(uuid_part[i])]
        rhs = BASE64_VALUES[ord(uuid_part[i+1])]
        
        result[Indices[j]] = HexChars[lhs >> 2]
        result[Indices[j+1]] = HexChars[((lhs & 0x03) << 2) | (rhs >> 4)]
        result[Indices[j+2]] = HexChars[rhs & 0x0F]
        j += 3
        
    return ''.join(result)

def compress_res_uuid(full_uuid):
    """ 压缩36字符UUID为22字符Base64格式 [8,11](@ref)"""
    uuid_part = full_uuid.split('@')[0].replace('-', '')
    if len(uuid_part) != 32:
        return full_uuid
    
    compressed = [uuid_part[0], uuid_part[1]]
    j = 2
    
    for i in range(2, 32, 3):
        left = HexMap[uuid_part[i]]
        mid = HexMap[uuid_part[i+1]]
        right = HexMap[uuid_part[i+2]]
        
        compressed.append(BASE64_KEYS[(left << 2) + (mid >> 2)])
        compressed.append(BASE64_KEYS[((mid & 0x03) << 4) + right])
        j += 2
        
    return ''.join(compressed)

# 测试用例
if __name__ == "__main__":
    uuids = [
        'da7782ed-d98d-4af4-82ab-1fb573a68e2d',
        '1739ef76-3d22-42ff-a591-2ec78dda91a7',
        '1b25647d-8a49-4c51-a021-c436aca8d3d8'
    ]
    for uuid in uuids:
        print(f"压缩前的 UUID: {uuid}, 压缩的 UUID: {compress_res_uuid(uuid)}")

    compress_uuids = [
        'dad4Lt2Y1K9IKrH7Vzpo4t',
        '17Oe92PSJC/6WRLseN2pGn',
        '1bJWR9iklMUaAhxDasqNPY'
    ]
    for uuid in compress_uuids:
        print(f"压缩前的 UUID: {decode_res_uuid(uuid)}, 压缩的 UUID: {uuid}")