import uuid
import base64

HexChars = list("0123456789abcdef")  # ['0','1',...,'f']

# 其他相关定义
BASE64_KEYS = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/="
BASE64_VALUES = {ord(c): i for i, c in enumerate(BASE64_KEYS)}
HexMap = {c: i for i, c in enumerate(HexChars)}  # 依赖 HexChars

def decode_script_uuid(compressed: str) -> str:
    head = compressed[:5]
    base64_part = compressed[5:]

    padding = '=' * ((4 - len(base64_part) % 4) % 4)
    base64_str = base64_part + padding

    try:
        byte_arr = base64.b64decode(base64_str)
    except Exception as e:
        raise ValueError(f"Invalid Base64 data: {e}")
    hex_str = byte_arr.hex()
    clean_hex = hex_str[:27]  # 移除末尾添加的 'f' 的影响

    # 组合头部和有效十六进制字符
    full_hex = head + clean_hex.ljust(27, '0')  # 确保长度27

    # 重组为标准 UUID 格式：8-4-4-4-12
    parts = [
        full_hex[:8],
        full_hex[8:12],
        full_hex[12:16],
        full_hex[16:20],
        full_hex[20:32]  # 若长度不足32，自动截断
    ]

    return try_fix_uuid('-'.join(parts)) # 不可逆，答案不准 

def try_fix_uuid(uuid_str: str) -> str:
    if verify_uuid_checksum(uuid_str):
        return uuid_str
    
    base_part = uuid_str[:-1]
    for c in HexChars:
        candidate = base_part + c
        if verify_uuid_checksum(candidate):
            return candidate
    raise ValueError("无法自动修复")

def crc8(data: bytes) -> int:
    """高性能CRC8校验（多项式0x8C）"""
    crc = 0
    for byte in data:
        crc ^= byte
        for _ in range(8):
            crc = ((crc << 1) ^ 0x8C) if (crc & 0x80) else (crc << 1)
            crc &= 0xFF
    return crc

def add_uuid_checksum(uuid_str: str) -> str:
    """为UUID最后字段添加校验码"""
    parts = uuid_str.split('-')
    last_part = parts[-1]
    
    # 计算前11字符的CRC8
    check_bytes = last_part[:11].encode('ascii')
    checksum = crc8(check_bytes)
    
    # 替换最后一位为校验码（十六进制字符）
    new_last = f"{last_part[:11]}{checksum & 0xF:01x}"
    return '-'.join(parts[:-1] + [new_last])

def verify_uuid_checksum(uuid_str: str) -> bool:
    """验证UUID校验码"""
    parts = uuid_str.split('-')
    last_part = parts[-1]
    
    if len(last_part) != 12:
        return False
    
    stored_checksum = int(last_part[-1], 16)
    check_bytes = last_part[:11].encode('ascii')
    return (crc8(check_bytes) & 0xF) == stored_checksum

def compress_script_uuid(uuid):
    head = uuid[:5]
    body = uuid[5:]
    body = body.replace('-','')+'f'
    intArr = []
    for x in range(len(body)-1):
        if x%2==0:
            intArr.append(int(body[x:x+2],16))
            pass
        pass
    return head+str(base64.b64encode(bytes(intArr)), "utf-8")[:-2]

# 测试用例
if __name__ == "__main__":
    uuids = [
        '2f402b34-55ea-408f-ad1e-dd6732d7428b',
        'd7aff29b-f325-4be4-b02a-482ec9e847cb',
        'ed85e8a4-91ef-4234-9cc5-76467eae6aa1',
        '9117a251-0e1d-4e13-8186-1bd043159540'
    ]
    for uuid in uuids:
        print(f"压缩前的 UUID: {uuid}, 压缩的 UUID: {compress_script_uuid(uuid)}")

    compress_uuids = [
        '2f402s0VepAj60e3Wcy10KL',
        'd7affKb8yVL5LAqSC7J6EfL',
        'ed85eikke9CNJzFdkZ+rmqh',
        '9117aJRDh1OE4GGG9BDFZVA'
    ]
    for uuid in compress_uuids:
        print(f"压缩前的 UUID: {decode_script_uuid(uuid)}, 压缩的 UUID: {uuid}")