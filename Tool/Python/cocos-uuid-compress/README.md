## Cocos Creator UUID 压缩及解压

在 Cocos Creator 中，获取资源或者脚本的UUID时，会提示如`[Window] UUID 已被复制： c06c9378-d75b-4c8d-b7d1-3bcc64407973 , 压缩的 UUID： c06c9N411tMjbfRO8xkQHlz`，其中UUID是随机生成的，而压缩的UUID是按照一定的规则对UUID进行压缩的。

资源的UUID压缩和脚本的UUID压缩是不同的。
* 使用 uuid_res_compress.py 对资源UUID进行压缩和解压。
```python
python uuid_res_compress.py
```

- UUID压缩
```python
compress_res_uuid('da7782ed-d98d-4af4-82ab-1fb573a68e2d') # dad4Lt2Y1K9IKrH7Vzpo4t
```
- UUID解压
```python
decode_res_uuid('17Oe92PSJC/6WRLseN2pGn') # 1739ef76-3d22-42ff-a591-2ec78dda91a7
```


* 使用 uuid_ts_compress.py 对脚本UUID进行压缩和解压。
```python
python uuid_ts_compress.py
```

- UUID压缩
```python
compress_ts_uuid('2f402b34-55ea-408f-ad1e-dd6732d7428b') # 2f402s0VepAj60e3Wcy10KL
```

- UUID解压
```python
# d7aff29b-f325-4be4-b02a-482ec9e847c0  正确为 # d7aff29b-f325-4be4-b02a-482ec9e847cb
# 因为脚本UUID是移位操作，解压时最后一位缺失无法准确还原，故而得到的UUID最后一位不准确
decode_ts_uuid('d7affKb8yVL5LAqSC7J6EfL') 
```
