import os

# the path of the file that you want to change the file's name
filePath = 'E:\\testAndroid352/assets/resources/scene30000'
# the list of the files.
list_path = os.listdir(filePath)

count = 1
for index in list_path:
    name = index.split('.')[0]
    kid = index.split('.')[-1]
    path = filePath + '\\' + index
    new_path = filePath + '\\scene-'+ str(count) + "." + kid
    count = count + 1
    os.rename(path, new_path)

print('Auto change the name completed!')