const fs = require('fs');
const path = require('path');
const images = require('images');

const LOG = '[icon-generate]';

// 扩展内定义的方法
exports.methods = {
    openPanel() {
        Editor.Panel.open('icon-generate');
    },
    async readConfig() {
        let config = await Editor.Profile.getProject('icon-generate', 'config');
        console.log(LOG, Editor.I18n.t('icon-generate.readConfigSuccess'));
        return config;
    },
    saveConfig(config) {
        Editor.Profile.setProject('icon-generate', 'config', config);
        console.log(LOG, Editor.I18n.t('icon-generate.saveConfigSuccess'));
        dealImage(config);
    }
};

const iconList = [
    // 安卓
    { url: path.join('android', 'mipmap-ldpi', 'ic_launcher.png'), width: 36, height: 36 },
    { url: path.join('android', 'mipmap-mdpi', 'ic_launcher.png'), width: 48, height: 48 },
    { url: path.join('android', 'mipmap-hdpi', 'ic_launcher.png'), width: 72, height: 72 },
    { url: path.join('android', 'mipmap-xhdpi', 'ic_launcher.png'), width: 96, height: 96 },
    { url: path.join('android', 'mipmap-xxhdpi', 'ic_launcher.png'), width: 144, height: 144 },
    { url: path.join('android', 'mipmap-xxxhdpi', 'ic_launcher.png'), width: 192, height: 192 },
    { url: path.join('android', 'playstore-icon.png'), width: 512, height: 512 },
    // IOS
    { url: path.join('ios', 'AppIcon.appiconset', "icon-20-ipad.png"), width: 20, height: 20 },
    { url: path.join('ios', 'AppIcon.appiconset', "icon-20@2x-ipad.png"), width: 40, height: 40 },
    { url: path.join('ios', 'AppIcon.appiconset', "icon-20@2x.png"), width: 40, height: 40 },
    { url: path.join('ios', 'AppIcon.appiconset', "icon-20@3x.png"), width: 60, height: 60 },
    { url: path.join('ios', 'AppIcon.appiconset', "icon-29-ipad.png"), width: 29, height: 29 },
    { url: path.join('ios', 'AppIcon.appiconset', "icon-29.png"), width: 29, height: 29 },
    { url: path.join('ios', 'AppIcon.appiconset', "icon-29@2x-ipad.png"), width: 58, height: 58 },
    { url: path.join('ios', 'AppIcon.appiconset', "icon-29@2x.png"), width: 58, height: 58 },
    { url: path.join('ios', 'AppIcon.appiconset', "icon-29@3x.png"), width: 87, height: 87 },
    { url: path.join('ios', 'AppIcon.appiconset', "icon-40.png"), width: 40, height: 40 },
    { url: path.join('ios', 'AppIcon.appiconset', "icon-40@2x.png"), width: 80, height: 80 },
    { url: path.join('ios', 'AppIcon.appiconset', "icon-40@3x.png"), width: 120, height: 120 },
    { url: path.join('ios', 'AppIcon.appiconset', "icon-60@2x.png"), width: 120, height: 120 },
    { url: path.join('ios', 'AppIcon.appiconset', "icon-60@3x.png"), width: 180, height: 180 },
    { url: path.join('ios', 'AppIcon.appiconset', "icon-76.png"), width: 76, height: 76 },
    { url: path.join('ios', 'AppIcon.appiconset', "icon-76@2x.png"), width: 152, height: 152 },
    { url: path.join('ios', 'AppIcon.appiconset', "icon-83.5@2x.png"), width: 167, height: 167 },
    { url: path.join('ios', 'AppIcon.appiconset', "icon-1024.png"), width: 1024, height: 1024 }
];


async function dealImage(config) {

    if (!fs.existsSync(config.image)) {
        let config = {
            type: 'error',
            title: 'error',
            detail: Editor.I18n.t('icon-generate.noImage')
        };
        Editor.Dialog['error'](Editor.I18n.t('icon-generate.error'), config);
        return;
    }

    if (null == config.output || '' == config.output) {
        let config = {
            type: 'error',
            title: 'error',
            detail: Editor.I18n.t('icon-generate.noOutput')
        };
        Editor.Dialog['error'](Editor.I18n.t('icon-generate.noOutput'), config);
        return;
    }

    if (fs.existsSync(path.join(config.output, 'android')) || fs.existsSync(path.join(config.output, 'ios'))) {
        let config = {
            type: 'info',
            title: 'info',
            detail: Editor.I18n.t('icon-generate.directorTip'),
            buttons: [Editor.I18n.t('icon-generate.continue'), Editor.I18n.t('icon-generate.cancel')]
        };
        const code = await Editor.Dialog['info'](Editor.I18n.t('icon-generate.directorTitle'), config);
        if (code == 1) {
            return;
        }
    }

    console.log(LOG, Editor.I18n.t('icon-generate.start'));
    for (let i = 0; i < iconList.length; i++) {
        let icon = iconList[i];
        mgm(config.image, path.join(config.output, icon.url), { width: icon.width, height: icon.height });
    }
    // IOS 描述文件
    fs.copyFileSync(path.join(__dirname, 'Contents.json'), path.join(config.output, 'ios', 'AppIcon.appiconset', 'Contents.json'));

    console.log(LOG, Editor.I18n.t('icon-generate.end'));
}

function mgm(src, dst, option) {
    let dstDir = path.parse(dst).dir;
    mkdir(dstDir);
    images(src)
        .size(option.width)
        .save(dst);
}

function mkdir(dirName, mode) {
    if (fs.existsSync(dirName)) {
        return true;
    }
    if (mkdir(path.dirname(dirName), mode)) {
        fs.mkdirSync(dirName, mode);
        return true;
    }
}

// 当扩展被启动的时候执行
exports.load = function () { };

// 当扩展被关闭的时候执行
exports.unload = function () { };