module.exports = {
    title: 'ICON生成器',
    openSetPannel: '打开设置窗口',
    selectTip: '支持png和jpg',
    selectTooltip: '建议上传1024像素获得最佳效果',
    continue: '继续',
    cancel: '取消',
    start: '开始制作',
    end: '制作完成',
    setPannel: '配置面板',
    selectImages: '选择图片',
    selectDirectory: '输出位置',
    generate: '生成',
    error: '生成错误',
    noImage: '请选择一张图片',
    noOutput: '请选择输出文件夹',
    directorTitle: '文件夹已存在',
    directorTip: '将会覆盖android和iOS文件夹'
}