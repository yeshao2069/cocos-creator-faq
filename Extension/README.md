
## 扩展

| NO. | Author | Desc | Proj | Mark |
| :---: | :---: | :---: | :---: | :---: |
| 1 | xu_yanfeng | 快速打开官方文档 | [cocos-helper](https://gitee.com/yeshao2069/cocos-creator-faq/tree/master/Extension/creator-helper) |  |
| 2 | dream93 | icon图标生成器 | [icon-generator](https://gitee.com/yeshao2069/cocos-creator-faq/tree/master/Extension/icon-generator) | [原文链接](https://forum.cocos.org/t/topic/109089) |
| 3 | VisualSJ | 股票小助手 | [stock-helper](https://gitee.com/yeshao2069/cocos-creator-faq/tree/master/Extension/stock-helper) | Creator 3.x |
| 4 | xu_yanfeng | Excel转JS和JSON | [excel-killer](https://gitee.com/yeshao2069/cocos-creator-faq/tree/master/Extension/excel-killer) | Creator 2.4.x、[原文链接](https://forum.cocos.org/t/topic/57352) |
| 5 | Cocos Creator | 检查模型2UV | [check2uv](https://gitee.com/yeshao2069/cocos-creator-faq/tree/master/Extension/check2uv) | Creator 3.4.x |
| 6 | muxiandong | 支持模型预览 | [fbxPreview24x](https://gitee.com/yeshao2069/cocos-creator-faq/tree/master/Extension/fbxPreview24x) | Creator 2.4.x |
| 7 | unknown | web 网页调试工具 | [preview-template](https://gitee.com/yeshao2069/cocos-creator-faq/tree/master/Extension/preview-template) | Creator 3.x |
| 8 | lixionglue | 查找源码循环引用-3.x | [refer-3x](https://gitee.com/yeshao2069/cocos-creator-faq/tree/master/Extension/refer-3x) | Creator 3.x |
| 9 | lixionglue | 查找源码循环引用-2.x | [refer-2x](https://gitee.com/yeshao2069/cocos-creator-faq/tree/master/Extension/refer-2x) | Creator 2.x |

### 使用方式
- 全局
MacOS 存放在系统下的 .CocosCreator 路径下的 extensions 文件夹，如 /Users/apple/.CocosCreator/extensions 文件夹下    
Window 存放在系统C盘 /User/用户/.CocosCreator路径下的extensions 文件夹，C:\Users/24754/.CocosCreator\extensions

- 项目
项目工程的 extensions 文件夹下

- 2.x插件
把 Extensions 文件夹替换成 packages 文件夹即可。