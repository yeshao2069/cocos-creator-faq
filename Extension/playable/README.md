
## playable

打包工程为`单文件html`

- playable-3x 支持 CocosCreator 3.x
- playable-2x 支持 CocosCreator 2.x

### 链接
https://github.com/ppgee/cocos-pnp/releases?q=playable-ads-adapter&expanded=true

### 使用方式
- playable-3.x 将插件解压后，放置到项目工程的 extensions 文件夹下，构建 web 包
- playable-2.x 将插件解压后，放置在项目工程的 packages 文件夹下，构建 web 包