
## 简介
基于 CocosCreator 2.4.8 版本创建的 **支持模型编辑器预览** 扩展。

------
### 使用说明
1、 拷贝 fbxPreview24x/package/fbx-preview 到项目  xxx/package下

2、 拷贝 fbxPreview24x/preview-templates 到项目 xxx/ 下

3、 拷贝 fbxPreview24x/assets/preview 到项目 xxx/assets 下


### 功能支持
1、 点击项目下的资源管理器的 FBX 可以预览 FBX

2、 预览的FBX 会自动使用同文件夹下的同名贴图（需要同文件路径、同名贴图）

3、 支持快捷键  command + shift + m  或者 ctrl + shift + m 打开扩展面板

4、 支持顶部菜单栏打开扩展面板

5、 支持扩展插件面板停靠属性面板

6、 支持 command + s 或者 ctrl + s 刷新当前的扩展面板

7、 支持鼠标 滚轮 放大缩小模型大小

8、 支持鼠标右键 开启模型跟随鼠标漫游

9、 支持鼠标左键 放置模型

10、支持模型左右旋转

11、 支持模型上下翻转

12、 支持自定义模型大小参数

### 备注
1、支持 FBX、fbx、jpg、png、JPG、PNG、jpeg、JPEG文件后缀