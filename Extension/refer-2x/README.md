# refer 2.x
Cocos Creator 2.x 源码循环引用查看。

## 使用方式

- 把扩展放在项目下的 packages 文件夹下，重启项目工程，在 CocosCreator 编辑器 -> 扩展 -> 查找源码循环依赖

![image](./screenshots/screenshot1.png)

## 前人种树

https://forum.cocos.org/t/topic/114754/7