
### Editor / 编辑器

---
### 1
- 咨询: 编辑器面板-项目设置-全局配置怎么使用
#### Version: Cocos Creator 3.x    

A: 
* ENABLE_TILEDMAP_CULLING

    是否开启 TiledMap 的自动裁减功能，默认开启。需要注意的是 TiledMap 如果设置了 skew 和 rotation 的话，建议手动关闭该项，否则会导致渲染出错。

* TOUCH_TIMEOUT

    用于甄别一个触点对象是否已经失效并且可以被移除的延时时长。开发者可通过修改这个值来获得想要的效果，默认值是 5000 毫秒。
    (API描述) 用于甄别一个触点对象是否已经失效并且可以被移除的延时时长 添加这个时长的原因是 X5 内核在微信浏览器中出现的一个 bug。 在这个环境下，如果用户将一个触点从底向上移出页面区域，将不会触发任何 touch cancel 或 touch end 事件，而这个触点会被永远当作停留在页面上的有效触点。 重复这样操作几次之后，屏幕上的触点数量将达到我们的事件系统所支持的最高触点数量，之后所有的触摸事件都将被忽略。 所以这个新的机制可以在触点在一定时间内没有任何更新的情况下视为失效触点并从事件系统中移除。 当然，这也可能移除一个真实的触点，如果用户的触点真的在一定时间段内完全没有移动（这在当前手机屏幕的灵敏度下会很难）。 你可以修改这个值来获得你需要的效果，默认值是 5000 毫秒。
    https://docs.cocos.com/creator/3.1/api/zh/modules/core.html#macro-1.touch_timeout

* ENABLE_TRANSPARENT_CANVAS

    用于设置 Canvas 背景是否支持 Alpha 通道，默认不开启支持。

    - 若希望 Canvas 背景是透明的，并显示背后的其他 DOM 元素，便可开启该项。
    - 若关闭该项，则会有更高的性能表现。

* ENABLE_WEBGL_ANTIALIAS

    用于设置在创建 WebGL Context 时是否开启抗锯齿选项，默认开启。


* CLEANUP_IMAGE_CACHE

    是否在将贴图上传至 GPU 之后删除原始图片缓存，删除之后图片将无法进行 动态合图。该项默认不开启

* ENABLE_MULTI_TOUCH

    是否开启多点触摸，默认开启。

* MAX_LABLE_CANVAS_POOL_SIZE

    设置 Label 使用的 Canvas 对象池的最大数量，请根据项目同场景的 Label 数量进行调整。

---
### 2
- 咨询: 怎么使用命令行打开工程 
#### Version: Cocos Creator All   
  
A: 
> 如果是2.x 项目的话，命令行打开项目
- /Applications/CocosCreator/Creator/2.4.7/CocosCreator.app/Contents/MacOS/CocosCreator --path /Users/mu/Desktop/NewProject_5
> 如果是3.x项目的话，命令行打开项目
- /Applications/CocosCreator/Creator/3.4.0/CocosCreator.app/Contents/MacOS/CocosCreator --project /Users/mu/work/gitee/GameOptimization3D --nologin

---
### 3
- 咨询: Cocos Creator 关闭自动刷新脚本
#### Version: Cocos Creator All

A: 
- 2.4.7+版本，可以在系统 ~/.CocosCreator/profiles/settings.json中，设置 "watch-file": false 即可。 可以通过 在开发者工具中 调用 Editor.assetdb.refresh('db://assets/脚本或者资源路径'); 进行手动刷新。
- 3.1.2版本后，可以通过 菜单 Cocos Creator -> 偏好设置 -> 资源数据库 -> 自动刷新资源 关闭。
可以通过插件的方式完成脚本刷新的功能。或者在开发者工具中 调用 Editor.Message.send('asset-db', 'refresh-asset', "db://assets/脚本或者资源路径"); 进行手动刷新。