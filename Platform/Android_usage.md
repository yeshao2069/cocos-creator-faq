
### Android - 安卓

---
### 1
- 咨询: AndroidStudio ADB 如何连接 MuMu 模拟器？
#### Version: Creator All

A:
* Windows10 系统
    - 进入到 Android SDK的 adb目录， 如 d://AndroidSDK/platform-tools 目录 (目录下有 adb.exe)
    - 输入命令 adb connect 127.0.0.1:7555 连接，连接成功会显示 connected to 127.0.0.1:7555。若是不成功，需要在 任务管理器 -> 详细信息 -> adb.exe -> 结束任务，重试。
    - 连接成功后，可以在Android Studio的设备管理里，找到并选择MuMu模拟器 Netease Mumu设备，连接到MuMu模拟器。

---
### 2
- 咨询: AndroidStudio ADB 如何连接夜神模拟器？
#### Version: Creator All

A: 
* Windows10 系统
    - 在 任务管理器 -> 详细信息 -> adb.exe -> 结束任务  (由于夜神开启的nox_adb.exe与AndroidStudio自带的adb.exe是冲突的，会导致夜神模拟器的adb无法开启)
    - 打开夜神模拟器，在菜单栏 -> 系统设置 -> 手机与网络 -> 预设机型中，可以看到当前模拟器的手机设备型号，如Samsung Galaxy S10
    - 打开Android Studio的设备管理里，找到并选择设备Samsung Galaxy S10，连接到夜神模拟器。

---
### 3
- 咨询: Android 利用 addr2line 工具定位奔溃行数？
#### Version: Creator All

A:     
* Windows10 系统
    - 需要发布后的libcocos.so文件。(这个可以通过解压apk包在lib目录下获得，如lib/x86)
    - 需要利用Android Studio ADB调试模拟器(MuMu模拟器、夜神模拟器)等，运行应用奔溃后，抓取日志保存为 a.txt文件
    - 抓到奔溃日志 backtrace处，可以看到奔溃的堆栈 (如果没有看到 pc xxxx xxxx的堆栈，你要检查一下日志的过滤模式，选择为no filter 不过滤), 在堆栈中找到具体的堆栈号， 如 008539f1 或者 00854bbb 这两个，没有具体的文件路径，我们通过工具来抓取一下
    - 在命令行输入  addr2line.exe的路径 -e libcocos.so的路径 -a 堆栈号，如 D:\AndroidSdk\ndk\21.1.6352462\toolchains\arm-linux-androideabi-4.9\prebuilt\windows-x86_64\bin\arm-linux-androideabi-addr2line.exe -e libcocos2dlua.so -a 008539f1
    - 可以看到具体指向的奔溃的行数
参考链接: https://blog.csdn.net/lcg_ryan/article/details/51614365

---
### 4
- 咨询: AndroidStudio 安装 release 包
#### Version: Creator All

A: 
    - AndroidStudio 连接安卓设备
    - AndroidStudio 界面，右下角找到设备文件管理器 Device File Explorer
    - 在设备文件管理器中 sdcard中放入 release的 apk 包 (选择sdcard中的文件夹然后右击选择upload 然后指定需要上传的apk的路径 打开)
    - 在手机的文件管理器中 找到apk包，安装即可。